var placeID = -1;
var new_usr_sex = -1;
var new_usr_place = -1;
var path = "../../Controller/php/General/OpsUsersDetails.php";
var today = moment().format('YYMMDD');
localStorage.setItem("FirstLoad",1);

function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
    });
}
function chooseMenu()
{
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{type:'menuType'},  
        dataType:"text",  
        success:function(d){
            var generalPath = "../../Controller/php/General/menu.php";
            switch(d){
                case 'Nutricionista':
                    ajax_call(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv");
                    ajax_call(generalPath,{type:'menuNutri'},"MenuContentDiv");
                break;
                case 'Administrador':
                    ajax_call(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv");
                    ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
                break;
            }
            ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
        }  
    });
}
function editColumn(e, dataNo, newValue, colName)
{
    if(e.keyCode == 13)
    {          
       edit_data(dataNo,newValue, colName);
    }
}
function edit_data(id, texto, column_name)  
{   
    $.post( path, { type:'update_user', object:'user', id: id, column_name:column_name, texto:texto } )
        .done(function(data) 
        {
            if(data > 0)
                highlightRow(id, "#8dc70a", column_name.substring(0, 2));
            else
                highlightRow(id, "#d64b2f", column_name.substring(0, 2));
        });
}
function highlightRow(rowId, bgColor, postfix)
{
    var rowSelector = $("#" + rowId + postfix);
    rowSelector.css("background-color", bgColor);
    rowSelector.fadeTo("normal", 0.5, function() { 
        rowSelector.fadeTo("fast", 1, function() { 
            rowSelector.css("background-color", '');
        });
    });
}
function disableEnter(e)
{
    if(e.keyCode == 13)
    {
        e.preventDefault();   
    }
} 
function previewURL(input) 
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#preview').css("background", "url(" + e.target.result +")" + " right top no-repeat");  
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function message(divName, message, iconType)
{
    if(divName === ''){
        $.notify(message,iconType,
        { 
            position:"bottom center",
            clickToHide: true
        });
    }else{
        $(divName).notify(message,iconType,
        { 
            position:"top center",
            clickToHide: true
        });
    }
    return '';
}
function init()  
{ 
    if(parseInt(localStorage.getItem("FirstLoad"))) 
    {
	localStorage.setItem("FirstLoad",0);
        chooseMenu();
    }
    ajax_call(path,{type:'all_images'},"all_images_div");
    ajax_call(path,{type:'showInformation'},"top_details_div");
    ajax_call(path,{type:'showBars'},"Bars_div");
    ajax_call(path,{type:'editableInformation'},"table_details_div");
    ajax_call(path,{type:'show_qr'},"qr_div");
    ajax_call(path,{type:'show_image'},"image_div");
    $('.selectpicker').selectpicker('show');
}   

$(document).ready(function()
{
    init();
    $('#tableBegin').DataTable({
        responsive: true
    });
    //------------------------------- DROP DOWN LISTS --------------------------
    $(document).on('changed.bs.select', '.selectpicker', function()
    {
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL === 'ddl_Genero_update'){
            ajax_call(path, { type:'update_user',  object:'user', id: $(this).data("id0"), column_name:'gender', texto:selected },"dummy_div");
        }
        else if(nameDDL === 'ddl_Sede_update'){
            ajax_call(path, { type:'update_user',  object:'session', id: $(this).data("id0"), column_name:'idPlace', texto:selected },"dummy_div");
            ajax_call(path,{type:'table_users',typePlace:'Specific',placeID:selected},"table_div");
            var title = $(this).find("[value="+selected+"]")[0].text;
            ajax_call(path,{type:'ddl_place_search', title:title},"filter_div");
            $('.selectpicker').selectpicker('show');
        }
    });
    //------------------------- EDIT ----------------------------------    
    $(document).on('keyup', '.class_name', function(e){  
        editColumn(e, $(this).data("id_name"), $(this).text(), "namePerson");
        ajax_call(path,{type:'table_users',typePlace:'Specific',placeID:placeID},"table_div");
    });
    $(document).on('keyup', '.class_nick', function(e){  
        editColumn(e, $(this).data("id_nick"), $(this).text(), "nickName");
        ajax_call(path,{type:'table_users',typePlace:'Specific',placeID:placeID},"table_div");
    });
    $(document).on('keyup', '.class_birthday', function(e){  
        editColumn(e, $(this).data("id_birthday"), $(this).text(), "birthday");
    });
    $(document).on('keyup', '.class_height', function(e){  
        editColumn(e, $(this).data("id_height"), $(this).text(), "height");
    });
    $(document).on('keyup', '.class_telephone', function(e){  
        editColumn(e, $(this).data("id_telephone"), $(this).text(), "telephone");
    });
    $(document).on('keyup', '.class_mail', function(e){  
        editColumn(e, $(this).data("id_mail"), $(this).text(), "mail");
    });
    //------------------------- DISABLE ENTER ----------------------------------
    $(document).on('keydown', 
    '.class_name,.class_nick,.class_birthday,.class_height,.class_telephone,.class_mail,.class_mail', 
    function(e){
        disableEnter(e);
    });
    //------------------------- UPDATE PROFILE PIC -----------------------------
    $(document).on('click', '#update_profile_pic', function(){
        previewURL(this);
        var nameImage = $("#ephoto-upload").val();
        if(nameImage != '') 
        {   
            //DB
            var UserId = -1;
            $.ajax({
                cache: false,
                async: false,
                url:path,  
                method:"POST",
                data:{
                    type:'add_user_pic',
                    ext:nameImage.slice(-3)
                },  
                dataType:"text",  
                success:function(d){  
                    UserId = d;
                }  
            });
            //FILE
            var formData = new FormData();
            formData.append('file', $("#ephoto-upload")[0].files[0]);
            $.ajax({
                url: '../../Controller/php/General/uploadImage.php?var='+UserId+'&url=../../../Multimedia/img/people/',
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (r) {
                    var res = r.split("#");
                    if(res[0]>0){
                        ajax_call(path,{type:'all_images'},"all_images_div");
                        ajax_call(path,{type:'show_image'},"image_div");
                        message("","La foto se ha actualizado satisfactoriamente.","success");
                    }
                    else{
                        message("","Lo sentimos, tu foto no se pudo actualizar. ".res[1],"danger");
                    }
                }
            });
        }
    }); 
    $(document).on('click', '#regenerateQR', function(){
        //qr-code
        var UserId = 1
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{type:'getUserId'},  
            dataType:"text",  
            success:function(d){  
                UserId = d;
            }  
        });
        var formData = new FormData();
        $.ajax({
            url: '../../Tools/qrcode/generate.php?var='+UserId,
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,  
            success:function(d){  
                message("","El QR de la persona se ha generado correctamente.","success");
            }  
        });
    }); 
    //------------------------- CALENDAR ---------------------------------------
    $('.class_birthday').datepicker({
        //todayBtn: true,
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
});
