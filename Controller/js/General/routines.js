var path = "../../Controller/php/General/OpsRoutines.php";
var categoryId = -1;
var placeId = -1;
var Going = [];
var allPlaces = false;
localStorage.setItem("FirstLoad",1);
    
function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function chooseMenu()
{
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{type:'menuType'},  
        dataType:"text",  
        success:function(d){
            var generalPath = "../../Controller/php/General/menu.php";
            switch(d){
                case 'Nutricionista':
                    ajax_call(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv");
                    ajax_call(generalPath,{type:'menuNutri'},"MenuContentDiv");
                break;
                case 'Administrador':
                    ajax_call(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv");
                    ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
                break;
            }
			ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
        }  
    });
}
function init()  
{
    if(parseInt(localStorage.getItem("FirstLoad"))) 
    {
	localStorage.setItem("FirstLoad",0);
        chooseMenu();
    }
    ajax_call(path,{type:'ddl_category'},"ddl_category_div");
    ajax_call(path,{type:'ddl_place'},"ddl_place_div");
//    $('.selectpicker').selectpicker('show');
}
function addPerson(newPerson,onlyOne)
{
    var flagNew = true;
    for (var index = 0; index < Going.length; index++) {
        var savedPerson = Going[index];
        //check if the person already exist
        if(savedPerson === newPerson){
            flagNew = false;
            if(Boolean(onlyOne))
                Going.splice(index, 1);
            break;
        }
    } 
    //add only if the person is new
    if(Boolean(flagNew))
        Going.push(newPerson);
}
function cleanAllCheck()
{
    $(".who").prop("checked", false);
    Going.length = 0;
}

$(document).ready(function() {
    init();
    // CALENDAR -----------------------------------------
    $('#txt_date_from,#txt_date_to').datepicker({
        //todayBtn: true,
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
    // SAVE ROUTINE ----------------------------------    
    $(document).on('click', '#btn_video', function(){
        var txt_title = $('#txt_title').val();
        var txt_link = $('#txt_link').val();
        var txt_date_from = $('#txt_date_from').val();
        var txt_date_to = $('#txt_date_to').val();
        
        if(txt_title === '')
            return alert('Sin Titulo');
        if(txt_link === '' && txt_link.length !== 44)
            return alert('Sin Link del Video');
        if(categoryId < 0)
            return alert('Sin Categoria');
        if(txt_date_from === '')
            return alert('Sin fecha inicial');
        if(txt_date_to === '')
            return alert('Sin fecha final');
        
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'upload_video'
                ,title:txt_title
                ,link:txt_link
                ,categoryId:categoryId
                ,date_from:txt_date_from
                ,date_to:txt_date_to
                ,Going:Going
                ,allPlaces:allPlaces
            },  
            dataType:"text",  
            success:function(d){  
                if(d>0){
                    $('#txt_title').val('');
                    $('#txt_link').val('');
                    $('#txt_date_from').val('');
                    $('#txt_date_to').val('');
                    $("#all_places_check").prop("checked", false);
                    init();
                    $('.selectpicker').selectpicker('show');
                    cleanAllCheck();
                    $('#user_table_div').html("");
                    alert('El video fué guardado satisfactoriamente');
                }
            }  
        });
    });
    // DROP DOWN LISTS -----------------------------------------
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL === 'ddl_category'){
            categoryId = selected;
        }else if(nameDDL === 'ddl_place'){
            placeId = selected;
            ajax_call(path,{type:'users_table_venue',placeID:selected},"user_table_div");
        }
    });
    // REGISTER CHECKBOX ----------------------------------
    $(document).on('click', '.who', function(e){
        //get the id
        var newPerson = $(this).val();
        //all the elemnts    
        if(newPerson == 0){
            var all = $('#mainCheck')[0].checked;
            if(Boolean(all)){
                $(".who").prop("checked", true);
                var full_table = document.getElementsByClassName("who");
                for (var index = 1; index < full_table.length; index++) {
                    var all_row = full_table[index].value;
                    addPerson(all_row,false);
                }
            }
            else
                cleanAllCheck();
        }//only one element
        else{
            //the first one
            if(Going.length == 0)
                Going.push(newPerson);
            else{ //otherwise for the rest of the elements
                $("#mainCheck").prop("checked", false);
                addPerson(newPerson,true);
            }
        }
    });
    // REGISTER CHECKBOX ----------------------------------
    $(document).on('click', '#all_places_check', function(e){
        allPlaces = $('#all_places_check')[0].checked;
        if(Boolean(allPlaces)){
            ajax_call(path,{type:'ddl_place'},"ddl_place_div");
            $('#user_table_div').html("");
            $('.selectpicker').selectpicker('show');
            $('#ddl_place').attr('disabled', true);
        }
        else
            $('#ddl_place').attr('disabled', false);
    });
});
