var path = "../../Controller/php/General/OpsNewContent.php";
var IdTypeContent = '';
var IdCategoryContent = -1;
var IsNew = 0;
var IdContent = -1;
localStorage.setItem("FirstLoad",1);

function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){
            $('#'+div).html(d);
        }  
   });
}
function chooseMenu()
{
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{type:'menuType'},  
        dataType:"text",  
        success:function(d){
            var generalPath = "../../Controller/php/General/menu.php";
            switch(d){
                case 'Nutricionista':
                    ajax_call(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv");
                    ajax_call(generalPath,{type:'menuNutri'},"MenuContentDiv");
                break;
                case 'Administrador':
                    ajax_call(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv");
                    ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
                break;
            }
			ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
        }  
    });
}
function init()  
{ 
    if(parseInt(localStorage.getItem("FirstLoad"))) 
    {
	localStorage.setItem("FirstLoad",0);
        chooseMenu();
    }
    ajax_call(path,{type:'ddl_type'},"type_ddl_div");
    $('.selectpicker').selectpicker('show');
    
    $('.summernote').summernote({
        height: 600,
        tabsize: 2
    });
    
    $('#titleNew').hide();
    $('#title_ddl_div').hide();
    $('#btn_new').hide();
    $('#txt_date_from').hide();
    $('#txt_date_to').hide();
}
function clean()
{
    $("#titleNew").val('');
    $(".summernote").summernote("code", '');
    $("#txt_date_from").val('');
    $("#txt_date_to").val('');  
}
function refreshTitleDdl()
{
    ajax_call(path,{type:'ddl_title',TypeContent:IdTypeContent,categoryContent:IdCategoryContent},"title_ddl_div");
    $('.selectpicker').selectpicker('show');
}

$(document).ready(function()
{
    init();
    //------------------------------- CALENDAR -----------------------------------------
    $('#txt_date_from,#txt_date_to').datepicker({
        //todayBtn: true,
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
    //------------------------------- SAVE CONTENT -----------------------------------------
    $('form').on('submit', function (e) {
        e.preventDefault();
        var data = $('.summernote').val();
        var title = $('#titleNew').val();
        var date_from = $('#txt_date_from').val();
        var date_to = $('#txt_date_to').val();
        //
        if(data === '') return;
        if(title === '') return;
        if(IdTypeContent === '') return;
        if(IdCategoryContent < 0) return;
        if(date_from === '') return;
        if(date_to === '') return;
        //
        if(IsNew > 0)
        {
            $.ajax({
                cache: false,
                async: false,
                url:path,  
                method:"POST",
                data:{
                    type:'save_content_db',
                    title:title, 
                    data:data,
                    typeId:IdTypeContent,
                    category:IdCategoryContent,
                    dateFrom:date_from,
                    dateTo:date_to
                },  
                dataType:"text",  
                success:function(d){
                    if(d > 0)
                    {
                        alert('Guardado correctamente');
                        clean();
                        ajax_call(path,{type:'save_content_file',title:title,data:data},"dummy_div");
                    }    
                    else
                        alert('Hubo un problema al guardar');                    
                }  
            });    
        }
        else
        {
            $.ajax({
                cache: false,
                async: false,
                url:path,  
                method:"POST",
                data:{
                    type:'update_content_db',
                    idCont:IdContent,
                    title:title, 
                    data:data,
                    typeId:IdTypeContent,
                    category:IdCategoryContent,
                    dateFrom:date_from,
                    dateTo:date_to
                },  
                dataType:"text",  
                success:function(d){
                    if(d == '')
                        alert('Hubo un problema al actualizar');
                    else{
                        alert('Actualizado correctamente');
                        ajax_call(path,{type:'save_content_file',title:title,data:data},"dummy_div");
                    }
                        
                }  
            }); 
        }
        //
        
    });
    //------------------------------- DROP DOWN LISTS -----------------------------------------
    $(document).on('changed.bs.select', '.selectpicker', function(){
        $("#txt_date_from").val('');
        $("#txt_date_to").val(''); 
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL === 'ddl_type')
        {
            IdTypeContent = selected;
            ajax_call(path,{type:'ddl_category',TypeContent:IdTypeContent},"category_ddl_div");
            
            $('#titleNew').hide();
            $('#title_ddl_div').hide();
            $('#btn_new').hide();
            $('#txt_date_from').hide();
            $('#txt_date_to').hide();
            
            $("#titleNew").val('');
            $('#title_ddl_div').val('');
            $('#txt_date_from').val('');
            $('#txt_date_to').val('');
        }
        else if(nameDDL === 'ddl_category')
        {
            IdCategoryContent = selected;
            refreshTitleDdl();
            
            $("#titleNew").hide();
            $('#title_ddl_div').show();
            $('#btn_new').show();
            $('#txt_date_from').show();
            $('#txt_date_to').show();
            
            $("#titleNew").val('');
            $('#txt_date_from').val('');
            $('#txt_date_to').val('');
        }
        else if(nameDDL === 'ddl_title')
        {
            IdContent = selected;
            if(IdContent > 0){
                clean();
                $.ajax({
                    cache: false,
                    async: false,
                    url:path,  
                    method:"POST",
                    data:{
                        type:'getContent'
                        ,idContent:IdContent
                    },  
                    dataType:"text",  
                    success:function(d){
                        if(d !== '')
                        {
                            var obj = JSON.parse(d);
                            $("#titleNew").val(obj[0][0]);
                            $(".summernote").summernote("code", obj[0][1]);
                            $("#txt_date_from").val(obj[0][2]);
                            $("#txt_date_to").val(obj[0][3]);                    
                        }
                    }  
                }); 
            }  
        }
        $('.selectpicker').selectpicker('show');
    });
    $(document).on('click', '#btn_new', function(){
        var title = $('#btn_new').text();
        if(title === "Nuevo")
        {
            IsNew = 1;
            $('#btn_new').text('Existente');
            $('#title_ddl_div').hide();
            $('#titleNew').show();
            $('#titleNew').val('');
            $('#txt_date_from').val('');
            $('#txt_date_to').val('');
            $(".summernote").summernote("code", '');
        }
        else if(title === "Existente")
        {
            IsNew = 0;
            $('#btn_new').text('Nuevo');
            refreshTitleDdl();
            $('#title_ddl_div').show();
            $('#titleNew').hide();
        }
        IdContent = -1;
    });
});    