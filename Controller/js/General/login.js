$(document).ready(function() {
    $('#login-form').validate({
        rules:{
            password: {
                required: true
            },
            user_email: {
                required: true,
                email: true
            },
        },
        messages:
        {
            password:{
                required: "Tu contraseña por favor"
            },
            user_email: "Un correo electrónico válido por favor",
       },
        submitHandler: submitForm 
    });  
});

function submitForm()
{  
    var data = $("#login-form").serialize();
    $.ajax({
        type : 'POST',
        url  : '../../Controller/php/General/login_process.php',
        data : data,
        beforeSend: function()
        { 
            $("#error").fadeOut();
            $("#btn-login").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; Comprobando');
        },
        success:function(response)
        {   
            var res = response.split("#");
            if(res[0]>0){
                $("#btn-login").html('<img src="../../Multimedia/img/loading_circle.gif"  width="20px" height="20px"/> &nbsp; Accediendo ...');
                switch(res[1].trim()){
                    case 'Integrante':
                        setTimeout('window.location.href="../User/Home.php";',1000);
                    break;
                    case 'Nutricionista':
                        setTimeout('window.location.href="../Nutricionist/home.php";',1000);
                    break;
                    case 'Administrador':
                        setTimeout('window.location.href="../Admin/home.php";',1000);
                    break;
                    case 'Entrenador':
                        setTimeout('window.location.href="../User/Appearance.php";',1000);
                    break;
                }
            }
            else{
                $("#error").fadeIn(2000, function(){      
                    $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
                    $("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Ingreso');
                });
            }
        }
    });
    return false;
}