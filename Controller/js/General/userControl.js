var placeID = -1;
var new_usr_sex = -1;
var new_usr_place = -1;
var idUser = -1;
var path = "../../Controller/php/General/OpsUsersControl.php";
var idNutriAppoint = -1;
var newMeasureType = '';
var newIdMeasureType = -1;
localStorage.setItem("FirstLoad",1);

function phpServer(sent_url,sent_data,div,async)
{
    $.ajax({
        cache: false,
        async: async,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
    });
}
function chooseMenu()
{
    $.ajax({
        cache: false,
        async: true,
        url:path,  
        method:"POST",
        data:{type:'menuType'},  
        dataType:"text",  
        success:function(d){
            var generalPath = "../../Controller/php/General/menu.php";
            switch(d){
                case 'Nutricionista':
                    phpServer(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv",true);
                    phpServer(generalPath,{type:'menuNutri'},"MenuContentDiv",true);
                break;
                case 'Administrador':
                    phpServer(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv",true);
                    phpServer(generalPath,{type:'menuAdmin'},"MenuContentDiv",true);
                break;
            }
            phpServer(generalPath,{type:'topBar'},"MenuTopDiv",true);
        }  
    });
}
function setLinear(lbl, data, name)
{
    var lineChartData = {
        labels : lbl,
        datasets : 
        [
            {
                label: name,
                backgroundColor: "rgba(183,102,25,0.5)",
                borderColor: "rgba(145,82,14,0.7)",
                data : data
            }
        ]
    };

    var ctx = document.getElementById("canvas-line").getContext("2d");
    ctx.canvas.height = 100;
    var myLineGraph = new Chart(ctx,    
    {
        type: "line",
        data: lineChartData, 
        responsive: true
    });
}
function message(divName, message, iconType)
{
    if(divName === ''){
        $.notify(message,iconType,
        { 
            position:"bottom center",
            clickToHide: true
        });
    }else{
        $(divName).notify(message,iconType,
        { 
            position:"top center",
            clickToHide: true
        });
    }
    return '';
}
function init()  
{ 
    if(parseInt(localStorage.getItem("FirstLoad"))) 
    {
	localStorage.setItem("FirstLoad",0);
        chooseMenu();
        phpServer(path,{type:'all_images'},"all_images_div",true);
        phpServer(path,{type:'showInformation'},"top_details_div",true);
    }
    phpServer(path,{type:'showBars'},"Bars_div",true);
    phpServer(path,{type:'indicators_ddl',title:'Cita'},"indicator_ddl_div",true);
    $('#btn_save_new').hide();
    $('#btn_cancel_new').hide();
    $('#btn_update').hide();
} 
function water_indicator(currentValue)
{
    var nameComponent = 'temp-gauge-0';
    var tempGauge = new Gauge({
        renderTo: nameComponent,
        width: 175,
        height: 175,
        glow: true,
        units: '%',
        title: 'Agua',
        minValue: 20,
        maxValue: 80,
        majorTicks: [ 20, 30, 40, 50, 60, 70, 80 ],
        minorTicks: 10,
        strokeTicks: false,
        highlights: [
            { from: 20, to: 50, color: 'rgba(255, 255, 0, .5)' }, //yellow
            { from: 50, to: 65, color: 'rgba(0, 255, 0, .5)' }, //green
            { from: 65, to: 80, color: 'rgba(255, 255, 0, .5)' }, //yellow
        ],
        colors: {
            plate: '#f5f5f5',
            majorTicks: '#666',
            minorTicks: '#888',
            title: '#444',
            units: '#000',
            numbers: '#222',
            needle: {
                start: 'rgba(240, 128, 128, 2)',
                end: 'rgba(255, 160, 122, .9)'
            }
        },
        animation: {
            delay : 25,
            duration: 500,
            fn : 'linear'
        },
        valueFormat: {
            int: 2,
            dec: 1
        }
    });
    tempGauge.setValue(currentValue);
    tempGauge.draw();
}
function fat_indicator(currentValue)
{
   var nameComponent = 'temp-gauge-1';
   var tempGauge = new Gauge({
        renderTo: nameComponent,
        width: 175,
        height: 175,
        glow: true,
        units: '%',
        title: 'Grasa',
        minValue: 5,
        maxValue: 40,
        majorTicks: [ 5, 10, 15, 20, 25, 30, 35, 40 ],
        //minorTicks: 10,
        strokeTicks: false,
        highlights: [
            { from: 5, to: 11, color: 'rgba(255, 255, 0, .5)' }, //yellow
            { from: 11, to: 22, color: 'rgba(0, 255, 0, .5)' }, //green
            { from: 22, to: 28, color: 'rgba(255, 255, 0, .5)' }, //yellow
            { from: 28, to: 40, color: 'rgba(255, 0, 0, .5)' } //red
        ],
        colors: {
            plate: '#f5f5f5',
            majorTicks: '#666',
            minorTicks: '#888',
            title: '#444',
            units: '#000',
            numbers: '#222',
            needle: {
                start: 'rgba(240, 128, 128, 2)',
                end: 'rgba(255, 160, 122, .9)'
            }
        },
        animation: {
            delay : 25,
            duration: 500,
            fn : 'linear'
        },
        valueFormat: {
            int: 2,
            dec: 1
        }
    });
    tempGauge.setValue(currentValue);
    tempGauge.draw();
}
function imc_indicator(currentValue)
{
   var nameComponent = 'temp-gauge-2';
   var tempGauge = new Gauge({
        renderTo: nameComponent,
        width: 175,
        height: 175,
        glow: true,
        units: 'kg/m2',
        title: 'IMC',
        minValue: 20,
        maxValue: 40,
        majorTicks: [ 20, 25, 30, 35, 40 ],
        strokeTicks: false,
        highlights: [
            { from: 20, to: 25, color: 'rgba(0, 255, 0, .5)' }, //green
            { from: 25, to: 30, color: 'rgba(255, 255, 0, .5)' }, //yellow
            { from: 30, to: 40, color: 'rgba(255, 0, 0, .5)' } //red
        ],
        colors: {
            plate: '#f5f5f5',
            majorTicks: '#666',
            minorTicks: '#888',
            title: '#444',
            units: '#000',
            numbers: '#222',
            needle: {
                start: 'rgba(240, 128, 128, 2)',
                end: 'rgba(255, 160, 122, .9)'
            }
        },
        animation: {
            delay : 25,
            duration: 500,
            fn : 'linear'
        },
        valueFormat: {
            int: 2,
            dec: 1
        }
    });
    tempGauge.setValue(currentValue);
    tempGauge.draw();
}
function vfr_indicator(currentValue)
{
   var nameComponent = 'temp-gauge-3';
   var tempGauge = new Gauge({
        renderTo: nameComponent,
        width: 175,
        height: 175,
        glow: true,
        units: 'cm2',
        title: 'VFR',
        minValue: 0,
        maxValue: 60,
        majorTicks: [0, 10, 20, 30, 40, 50, 60 ],
        //minorTicks: 10,
        strokeTicks: false,
        highlights: [
            { from: 0, to: 12, color: 'rgba(0, 255, 0, .5)' }, //green
            { from: 12, to: 60, color: 'rgba(255, 0, 0, .5)' } //red
        ],
        colors: {
            plate: '#f5f5f5',
            majorTicks: '#666',
            minorTicks: '#888',
            title: '#444',
            units: '#000',
            numbers: '#222',
            needle: {
                start: 'rgba(240, 128, 128, 2)',
                end: 'rgba(255, 160, 122, .9)'
            }
        },
        animation: {
            delay : 25,
            duration: 500,
            fn : 'linear'
        },
        valueFormat: {
            int: 2,
            dec: 1
        }
    });
    tempGauge.setValue(currentValue);
    tempGauge.draw();
}
function showInfoCurrentAppoint(_idNutriAppoint,_measureType)
{
    //general info
    phpServer(path,{
        type:'indicators_past_info'
        ,idNutriAppoint:_idNutriAppoint
    },"indicator_past_div",false);
    //table
    phpServer(path,{
        type:'indicators_table'
        ,idNutriAppoint:_idNutriAppoint
        ,measureType:_measureType
    },"indicator_table_div",true);
    //watches
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'tanita'
        },  
        dataType:"text",  
        success:function(d){
            if(d != ''){
                var obj = JSON.parse(d);
                water_indicator(obj[0][1]);
                fat_indicator(obj[1][1]);
                imc_indicator(obj[2][1]);
                vfr_indicator(obj[3][1]);
            }
        }  
    });
}

$(document).ready(function(){
    init();
});
//show
$(document).on('changed.bs.select', '.selectpicker', function()
{
    var nameDDL = this.id;
    var selected = $(this).find("option:selected").val();
    if(nameDDL === 'ddl_indicators')
    {
        $('#ddl_type_appoint_div').html('');    
        var res = selected.split("#"); 
        idNutriAppoint = res[0];
        showInfoCurrentAppoint(idNutriAppoint,res[1]);
        $('.changeDate').datepicker({
            language: "es",
            autoclose: true,
            format: 'dd/mm/yyyy'
        });
        $('.changeHour').clockpicker({
            placement: 'bottom',
            align: 'left',
            donetext: 'Listo'
        });
        $('#btn_update').show();
    }
    else if(nameDDL === 'ddl_measure_type'){ 
        var res = selected.split("#"); 
        newIdMeasureType = res[0];
        newMeasureType = res[1].split(" ")[1];
    }
});
//show graph
$(document).on('click', '.indicatorSee', function()
{  
    var idIndicator = $(this).data("id1");
    var obj;
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'lectures', 
            idIndicator:idIndicator
        },  
        dataType:"text",  
        success:function(d){
            if(d != ''){
                obj = JSON.parse(d);
                var lbl = new Array(obj.length-1);
                var dat = new Array(obj.length-1);
                for(var j = 0; j < obj.length; j++){
                    lbl[j] = obj[j][0];
                    dat[j] = obj[j][1];
                }
                phpServer(path,{type:'refresh_canvas'},"canvas_div",false);
                setLinear(lbl,dat,obj[0][2]);
            }
        }  
    });
});
//new appoint
$(document).on('click', '#btn_new_appoint', function(e)
{  
    //OLD indicator_ddl_div
    //NEW ddl_type_appoint_div
    var title = $('#btn_new_appoint').text();
    if(title === "Nueva")
    {
        $('#btn_new_appoint').text('Existente');
        $('#indicator_past_div').html('');
        $('#indicator_table_div').html('');
        $('#indicator_ddl_div').html('');
        phpServer(path,{type:'appoint_info'},"ddl_type_appoint_div",false);
        $('#btn_save_new').show();
        $('#btn_cancel_new').show();
        $('#btn_update').hide();
    }
    else if(title === "Existente")
    {
        $('#btn_new_appoint').text('Nueva');
        phpServer(path,{type:'indicators_ddl',title:'Cita'},"indicator_ddl_div",true);
        $('#ddl_type_appoint_div').html('');
        $('#btn_save_new').hide();
        $('#btn_cancel_new').hide();
        $('#btn_update').show();
    }
    $('#canvas_div').html('');
    $('.selectpicker').selectpicker('show');
});
$(document).on('click', '#btn_save_new', function(e)
{  
    if(newIdMeasureType < 0) {message("", "No ha seleccionado el tipo de medición", "danger"); return;}
    $.ajax({
        cache: false,
        async: true,
        url:path,  
        method:"POST",
        data:{
            type:'add_same_day_appoint',
            newIdMeasureType:newIdMeasureType
        },  
        dataType:"text",  
        success:function(newId){  
            if(parseInt(newId)>0){
                message("", "La cita se ha guardado con éxito", "success");
                idNutriAppoint = newId;
                $('#ddl_type_appoint_div').html('');    
                showInfoCurrentAppoint(newId,newMeasureType);
                $('#btn_save_new').hide();
                $('#btn_cancel_new').hide();
                $.ajax({
                    cache: false,
                    async: true,
                    url:path,  
                    method:"POST",
                    data:{
                        type:'titleNewAppoint',
                        idNewAppoint:newId
                    },  
                    dataType:"text",  
                    success:function(newTitle){
                        phpServer(path,{type:'indicators_ddl',title:newTitle},"indicator_ddl_div",false);
                        $('.selectpicker').selectpicker('show');
                    }
                });
            }
            else
            {
                message("", "Hubo un problema al guardar la cita", "danger");   
            }
        }  
    });
});
$(document).on('click', '#btn_cancel_new', function(e)
{  
    $('#indicator_past_div').html('');
    $('#indicator_table_div').html('');
    $('#indicator_ddl_div').html('');
    phpServer(path,{type:'appoint_info'},"ddl_type_appoint_div",true);
    $('.selectpicker').selectpicker('show');
    newIdMeasureType = -1;
});
//edit data and points
$(document).on('click', '#btn_update', function(e){
    var updateDate = $('#updateFecha').val();
    var updateHour = $('#updateHora').val();
    var updatePoints = $('#updatePuntos').text();
    var idNutriAppoint = $('.assignPoints').data("id0");
    
    if(updateDate === '') {message("", "No ha seleccionado una fecha", "danger"); return;}
    if(updateHour === '') {message("", "No ha seleccionado una hora", "danger"); return;}
    if(updatePoints < 0) {message("", "No ha seleccionado los puntos", "danger"); return;}
    
    $.ajax({
        cache: false,
        async: true,
        url:path,  
        method:"POST",
        data:{
            type:'assign_date_points'
            ,idNutriAppoint:idNutriAppoint
            ,date:updateDate
            ,hour:updateHour
            ,points:updatePoints
        },  
        dataType:"text",  
        success:function(d){
            if(d > 0){
                phpServer(path,{type:'indicators_ddl',title:'Cita'},"indicator_ddl_div",false);
                $('.selectpicker').selectpicker('show');
                message("", "La cita se ha actualizado con éxito", "success");
            }else
                message("", "La cita no se ha podido actualizar", "danger");
        }  
    });
});
