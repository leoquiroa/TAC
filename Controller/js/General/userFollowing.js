var placeID = -1;
var new_usr_sex = -1;
var new_usr_place = -1;
var idUser = -1;
var path = "../../Controller/php/General/OpsUsersFollowing.php";
localStorage.setItem("FirstLoad",1);

function phpServer(sent_url,sent_data,div,async){
    $.ajax({
        cache: false,
        async: async,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
    });
}
function chooseMenu(){
    $.ajax({
        cache: false,
        async: true,
        url:path,  
        method:"POST",
        data:{type:'menuType'},  
        dataType:"text",  
        success:function(d){
            var generalPath = "../../Controller/php/General/menu.php";
            switch(d){
                case 'Nutricionista':
                    phpServer(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv",true);
                    phpServer(generalPath,{type:'menuNutri'},"MenuContentDiv",true);
                break;
                case 'Administrador':
                    phpServer(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv",true);
                    phpServer(generalPath,{type:'menuAdmin'},"MenuContentDiv",true);
                break;
            }
            phpServer(generalPath,{type:'topBar'},"MenuTopDiv",true);
        }  
    });
}
function message(divName, message, iconType){
    if(divName === ''){
        $.notify(message,iconType,
        { 
            position:"bottom center",
            clickToHide: true
        });
    }else{
        $(divName).notify(message,iconType,
        { 
            position:"top center",
            clickToHide: true
        });
    }
    return '';
}
function cleanFirstNote(){
    $("#txtReason").val('');
    $("#txtGlucosaBgn").val('');
    $("#txtGlucosaEnd").val('');
    $("#txtNote").val('');
}
function disableEnter(e){
    if(e.keyCode == 13)
    {
        e.preventDefault();   
    }
} 
function editColumn(e, dataNo,texto){
    if(e.keyCode == 13)
    {          
       edit_data(dataNo,texto);
    }
}
function edit_data(idNote, texto)  {   
    var res = idNote.split('#');
    if(res[1] === 'description'){
        $.post(path,{ 
            type:'updateNoteDescription', 
            idNoteDescription: res[0],
            texto: texto
        }).done(function(data) 
        {
            if(data > 0)
                highlightRow(res[1], res[0], "#8dc70a");
            else
                highlightRow(res[1], res[0], "#d64b2f");
        });
    }
    else{
        $.post(path,{ 
            type:'updateNoteSession', 
            idNoteSession: res[0], 
            column: res[1],
            value: texto
        }).done(function(data) 
        {
            if(data > 0)
                highlightRow(res[1], '', "#8dc70a");
            else
                highlightRow(res[1], '', "#d64b2f");
        });
    }
    
}
function highlightRow(element, rowId, bgColor){
    var rowSelector = $("#" + element + rowId);
    rowSelector.css("background-color", bgColor);
    rowSelector.fadeTo("normal", 0.5, function() { 
        rowSelector.fadeTo("fast", 1, function() { 
            rowSelector.css("background-color", '');
        });
    });
}
function init(){ 
    if(parseInt(localStorage.getItem("FirstLoad"))) 
    {
	localStorage.setItem("FirstLoad",0);
        chooseMenu();
        phpServer(path,{type:'all_images'},"all_images_div",true);
        phpServer(path,{type:'showInformation'},"top_details_div",true);
        phpServer(path,{type:'showBars'},"Bars_div",true);
    }
    phpServer(path,{type:'FollowingNotes'},"notes_div",true);
}   

$(document).ready(function(){
    init();
});
$(document).on('click', '#btnNewNote', function(){
        var reason = $("#txtReason").val();
        var glucosaBgn = $("#txtGlucosaBgn").val();
        var glucosaEnd = $("#txtGlucosaEnd").val();
        var note = $("#txtNote").val();
        if(reason === '')
            message("","Porfavor ingresa una razón.","warn");
        else if(glucosaBgn === '')
            message("","Porfavor ingresa una medida inicial de Glucosa.","warn");
        else{
            $.ajax({
                cache: false,
                async: true,
                url:path,  
                method:"POST",
                data:{
                    type:'AddFirstNote',
                    reason:reason,
                    glucosaBegin:glucosaBgn,
                    glucosaEnd:glucosaEnd,
                    description:note
                },  
                dataType:"text",  
                success:function(d){  
                    if(d){
                        cleanFirstNote();
                        phpServer(path,{type:'FollowingNotes'},"notes_div",true);
                    }
                    else
                        message("","Hubo un problema al guardar tu nota.","warn");
                }  
            });
        }
    });
$(document).on('click', '#btnMoreNotes', function(){
        var AnotherNote = $("#txtAnotherNote").val();
        if(AnotherNote === '')
            message("","Porfavor ingresa una nota nueva.","warn");
        else{
            $.ajax({
                cache: false,
                async: true,
                url:path,  
                method:"POST",
                data:{
                    type:'AddMoreNotes',
                    description:AnotherNote
                },  
                dataType:"text",  
                success:function(d){  
                    if(d){
                        $("#txtAnotherNote").val('');
                        phpServer(path,{type:'FollowingNotes'},"notes_div",true);
                    }
                    else
                        message("","Hubo un problema al guardar tu nota.","warn");
                }  
            });
        }
    });
$(document).on('click', '.deleteNote', function(){
        idNote = $(this).data("id1");
        $.ajax({
                cache: false,
                async: true,
                url:path,  
                method:"POST",
                data:{
                    type:'deleteDescriptionNote',
                    idNote:idNote
                },  
                dataType:"text",  
                success:function(d){  
                    if(d){
                        phpServer(path,{type:'FollowingNotes'},"notes_div",true);
                    }
                    else
                        message("","Hubo un problema al eliminar la nota.","warn");
                }  
            });
    });
$(document).on('keyup', '.editNote,.editReason,.editGlucoseBegin,.editGlucoseFinal', 
function(e){  
    editColumn(e, $(this).data("id1"), $(this).text());
    phpServer(path,{type:'table_users',typePlace:'Specific',placeID:placeID},"table_div",true);
});
$(document).on('keydown', '.editNote,.editReason,.editGlucoseBegin,.editGlucoseFinal', 
function(e){
    disableEnter(e);
});