var path = "../../Controller/php/General/OpsEvents.php";
var categoryId = -1;
localStorage.setItem("FirstLoad",1);

//------------------------- AJAX CALL ----------------------------------
function list_ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function chooseMenu(){
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{type:'menuType'},  
        dataType:"text",  
        success:function(d){
            var generalPath = "../../Controller/php/General/menu.php";
            switch(d){
                case 'Nutricionista':
                    list_ajax_call(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv");
                    list_ajax_call(generalPath,{type:'menuNutri'},"MenuContentDiv");
                break;
                case 'Administrador':
                    list_ajax_call(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv");
                    list_ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
                break;
            }
			list_ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
        }  
    });
}
//------------------------------- SHOW -----------------------------------------
function fetch_data()  
{
    if(parseInt(localStorage.getItem("FirstLoad"))) 
    {
	localStorage.setItem("FirstLoad",0);
        chooseMenu();
    }
    list_ajax_call(path,{type:'ddl_category'},"category_div");
    $('.clockpicker').clockpicker({
        placement: 'top',
        align: 'left'
    });
    $("#input_hour").show();
}
function cleanWindow(){
    $('#txt_title').val('');
    $('#txt_date').val('');
    $('#txt_place').val('');
    $('#txt_hour').val('');
    fetch_data();
    $('#txt_date_from').val('');
    $('#txt_date_to').val('');
    $(".fileinput").fileinput("clear");
}

$(document).ready(function() {
    fetch_data();
    //------------------------------- CALENDAR -----------------------------------------
    $('#txt_date_from,#txt_date_to,#txt_date').datepicker({
        //todayBtn: true,
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
    //------------------------- BUTTON ----------------------------------
    $(document).on('click', '#btn_events', function(e){
        var title = $('#txt_title').val();
        var date = $('#txt_date').val();
        var place = $('#txt_place').val();
        var hour = $('#txt_hour').val();
        var date_from = $('#txt_date_from').val();
        var date_to = $('#txt_date_to').val();
        var file_data = $('#ephoto-upload').prop('files')[0];   
        
        if(file_data === undefined)
            return alert('Sin archivo');
        if(title === '')
            return alert('Sin titulo del evento');
        if(date == '')
            return alert('Sin fecha del evento');
        if(place == '')
            return alert('Sin lugar del evento');
        if(hour == '')
            return alert('Sin Hora');
        if(categoryId < 0)
            return alert('Sin categoria del evento');
        if(date_from == '')
            return alert('Sin fecha inicial');
        if(date_to == '')
            return alert('Sin fecha final');
        
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{ 
                type:'save_event',  
                title:title, 
                datetime: date + ' ' + hour,
                place: place, 
                categoryId: categoryId, 
                date_from: date_from,
                date_to: date_to
            },  
            dataType:"text",  
            success:function(newId){
                if(newId > 0 &&  $("#ephoto-upload").val()!==''){
                    var formData = new FormData();
                    formData.append('file', $("#ephoto-upload")[0].files[0]);
                    $.ajax({
                        url: '../../Controller/php/General/uploadImage.php?var='+newId+'&url=../../../Multimedia/img/flyers/',
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (r) {
                            var res = r.split("#");
                            if(res[0]>0){
                                list_ajax_call(path, { 
                                    type:'update_event', 
                                    id:newId, 
                                    column_name:'flyerPath', 
                                    texto:newId+'.'+res[1]},
                                "dummy_div");
                                cleanWindow();
                                alert("El evento se ha guardado satisfactoriamente.");
                            }
                            else{
                                alert("Lo sentimos, tu hubo un problema al guardar la foto del nuevo evento. "+res[1]);
                            }
                        }
                    });
                }
            }
        });
    });
    //------------------------- SELECT ----------------------------------
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL === 'ddl_category'){
            categoryId = selected;
        }
    }); 
});
