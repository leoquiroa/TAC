var dateBgn = '';
var placeID = -1;
var path = "../../Controller/php/Administration/OpsStatistics.php";
//------------------------- AJAX CALL ----------------------------------
function list_ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function session_ajax_call(sent_url,sent_data,type){
    $.ajax({
        cache: true,
        async: true,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(A){
            var obj = JSON.parse(A);
            if(type === 'Movement'){
                setPie(obj);
                list_ajax_call(path,{type:'PeopleMovementTable',data:obj},"table-pie");
            }
            else if(type === 'Gender'){
                setDoughnut(obj);
                list_ajax_call(path,{type:'GenderAttendTable',data:obj},"table-doughnut");
            }
            else if(type === 'Schedule'){
                var lbl = new Array(1);
                var pts = new Array(1);
                var j = 0;
                for (var x in obj) {
                    lbl[j] = obj[x][0];
                    pts[j] = obj[x][1];
                    j++;
                }
                setLinear(lbl,pts);
                list_ajax_call(path,{type:'ScheduleTable',data:obj},"table-line");
            }
            else if(type === 'Class'){
                var lbl = new Array(1);
                var pts = new Array(1);
                var j = 0;
                for (var x in obj) {
                    lbl[j] = obj[x][0];
                    pts[j] = obj[x][1];
                    j++;
                }
                setBar(lbl,pts);
                list_ajax_call(path,{type:'ClassTable',data:obj},"table-bar");
            }
        }  
    });
}
//------------------------------- SHOW -----------------------------------------
function fetch_data(){ 
    var generalPath = "../../Controller/php/General/menu.php";
    list_ajax_call(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv");
    list_ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
    list_ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
    list_ajax_call(path,{type:'ddl_place_date'},"filter_div");
}
//------------------------------- DATE -----------------------------------------
function fn_getMonth(m){
    //return parseInt(m.getUTCMonth()+1);
    return parseInt(m.getMonth()+1);
}
function fn_getYear(y){
    return y.getUTCFullYear();
}
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}
//------------------------------- GRAPHS -----------------------------------------    
function setLabels(xLabel, yLabel){
    var lbl = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                },
                scaleLabel: {
                    display: true,
                    labelString: yLabel
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: xLabel
                }
            }]
        },
        responsive: true
    };
    return lbl;
}
function setPie(obj){
    var pieData = {
        labels: [obj[1][0], obj[2][0], obj[3][0]],
        datasets: [
            {
                data: [obj[1][1], obj[2][1], obj[3][1]],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56"
                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56"
                ]
            }
        ]
    };
    var ctx = document.getElementById("chart-pie").getContext("2d");
    ctx.canvas.height = 150;
    new Chart(ctx,    
    {
        type: "pie",
        data: pieData, 
        responsive: true
    });
}    
function setDoughnut(obj){        
    var doughnutData = {
        labels: [
            obj[1][0],
            obj[2][0]
        ],
        datasets: [
            {
                data: [obj[1][1], obj[2][1]],
                backgroundColor: [
                    "#4D5360",
                    "#F7464A"
                ],
                hoverBackgroundColor: [
                    "#616774",
                    "#FF5A5E"
                ]
            }
        ]
    };
    var ctx = document.getElementById("chart-doughnut").getContext("2d");
    ctx.canvas.height = 150;
    new Chart(ctx,    
    {
        type: "doughnut",
        data: doughnutData, 
        responsive: true
    });
}
function setLinear(lbl, linearData){
    var lineChartData = {
        labels : lbl,
        datasets : [
            {
                label: "Número de Personas",
                backgroundColor : "rgba(77,132,221,0.5)",
                data : linearData
            }
        ]
    };
    var ctx = document.getElementById("canvas-line").getContext("2d");
    ctx.canvas.height = 150;
    new Chart(ctx,    
    {
        type: "line",
        data: lineChartData, 
        options: setLabels('Hora', 'Personas')
    });
}
function setBar(lbl,barData){
    var barChartData = {
    labels : lbl,
    datasets : [
            {
                label: "Número de Personas",
                backgroundColor : "rgba(255, 159, 64, 0.2)",
                data : barData
            }
        ]
    };
    var ctx = document.getElementById("chart-bar").getContext("2d");
    ctx.canvas.height = 150;
    new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: setLabels('Actividad', 'Puntos')
    });
}
//------------------------- MESSAGE ALERT ----------------------------------
function messages(msg,clss){
    $('#myAlert').attr("class", clss);
    $('#myAlert').text(msg);
    $('#myAlert').show('slow');
    setTimeout ( function () {$('#myAlert').hide('slow');}, 2000);
}
//------------------------- MESSAGE ALERT ----------------------------------
function drawTablesAndGraphs(){
    list_ajax_call(path,{type:'graphPeople'},"graphPeople_div");
    session_ajax_call(path,{type:'PeopleMovementGraph',dateBgn:dateBgn,placeID:placeID},'Movement');

    list_ajax_call(path,{type:'graphGender'},"graphGender_div");
    session_ajax_call(path,{type:'GenderAttendGraph',dateBgn: dateBgn,placeID: placeID},'Gender');

    list_ajax_call(path,{type:'graphSchedule'},"graphSchedule_div");
    session_ajax_call(path,{type:'ScheduleGraph',dateBgn: dateBgn,placeID: placeID},'Schedule');

    list_ajax_call(path,{type:'graphClass'},"graphClass_div");
    session_ajax_call(path,{type:'ClassGraph',dateBgn: dateBgn,placeID: placeID},'Class');
}
$(document).ready(function(){
    fetch_data();
    //------------------------- CALENDAR DATE PICKER ----------------------------------
    $('#time_span').datepicker({
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
    //------------------------------- DROP DOWN LIST -----------------------------------------
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL === 'ddl_place'){
            placeID = selected;
            if(dateBgn !== '')
                drawTablesAndGraphs();
        }
    });
    //------------------------------- DATE PICKER -----------------------------------------
    $('#time_span').datepicker().on('changeDate', function(ev){
        if( $('#time_span').val() !== ''){
            var selected1 = addDays(ev.date, 1);
            dateBgn = fn_getYear(selected1) + '-' + fn_getMonth(selected1) + '-01';
            if(placeID > 0)
                drawTablesAndGraphs();
        }
    });  
});