/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    $(document).on('click', '#star1', function(){
        $('.star-set').attr("class", "glyphicon glyphicon-star-empty star-set");
        $('#star1').attr("class", "glyphicon glyphicon-star star-set");
        $('#rate_text').text('1');
    });
    
    $(document).on('click', '#star2', function(){
        $('.star-set').attr("class", "glyphicon glyphicon-star-empty star-set");
        $('#star1').attr("class", "glyphicon glyphicon-star star-set");
        $('#star2').attr("class", "glyphicon glyphicon-star star-set");
        $('#rate_text').text('2');
    });
    
    $(document).on('click', '#star3', function(){
        $('.star-set').attr("class", "glyphicon glyphicon-star star-set");
        $('#star4').attr("class", "glyphicon glyphicon-star-empty star-set");
        $('#star5').attr("class", "glyphicon glyphicon-star-empty star-set");
        $('#rate_text').text('3');
    });
    
    $(document).on('click', '#star4', function(){
        $('.star-set').attr("class", "glyphicon glyphicon-star star-set");
        $('#star5').attr("class", "glyphicon glyphicon-star-empty star-set");
        $('#rate_text').text('4');
    });
    
    $(document).on('click', '#star5', function(){
        $('.star-set').attr("class", "glyphicon glyphicon-star star-set");
        $('#rate_text').text('5');
    });
});