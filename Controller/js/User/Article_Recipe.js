var typeRecipeID = -1;
var typeArticleID = -1;
var nameDDL = '';
var path = "../../Controller/php/User/OpsArticleRecipe.php";

function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init()  
{
    ajax_call(path,{type:'ddl_article'},"article_category_dll");
    ajax_call(path,{type:'fav_rate_Articulos',cont:'Articulos'},"table_Articulos_Fav_Rate_div");
    ajax_call(path,{type:'signs_fav_rate_article'},"signs_Articulos_fav_rate_div");
    ajax_call(path,{type:'ddl_recipe'},"recipe_category_dll");
    ajax_call(path,{type:'fav_rate_Recetas',cont:'Recetas'},"table_Recetas_Fav_Rate_div");
    ajax_call(path,{type:'signs_fav_rate_recipe'},"signs_Recetas_fav_rate_div");
    $('.selectpicker').selectpicker('show');
}

$(document).ready(function() {
    init();
    $(document).on('changed.bs.select', '.selectpicker', function()
    {
        nameDDL = this.id;
        var selected = $(this).val();
        if(nameDDL == 'ddl_type_article')
        {
            typeArticleID = selected;
            $('#tmp_category').val(selected);
            ajax_call(path,
                {
                    type:'table_article',
                    typeArticleID:typeArticleID,
                    typeRecipeID:typeRecipeID
                },
                "table_article_div"); 
        }
        else if(nameDDL == 'ddl_type_recipe')
        {
            typeRecipeID = selected;
            $('#tmp_recipe').val(selected);
            ajax_call(path,
                {
                    type:'table_recipe',
                    typeArticleID:typeArticleID,
                    typeRecipeID:typeRecipeID
                },
                "table_recipe_div"); 
        }
    });
});


