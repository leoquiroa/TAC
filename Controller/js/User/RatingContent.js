function getPath(label){
    var path = "";
    switch(label){
        case 'Videos':
            path = "../../Controller/php/User/OpsRoutine.php";
        break;
        case 'Recetas': case 'Articulos':
            path = "../../Controller/php/User/OpsArticleRecipe.php";
        break;
        case 'Esfuerzo': case 'Dietas':
            path = "../../Controller/php/User/OpsDietEffort.php";
        break;
    }
    return path;
}
function phpServer(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}

//FAV CONTENT ACTION
$(document).on('click', '.favContent', function(){
    var id0 = $(this).data("id0");
    var info = id0.split("%");
    var fav = "fa fa-bookmark fa-2x favContent";
    var notfav = "fa fa-bookmark-o fa-2x favContent";
    if($('#'+info[1]+'-'+info[0]).attr("class") === fav)
    {
        $('#'+info[1]+'-'+info[0]).attr("class",notfav);
        phpServer(getPath(info[1]),{type:'NotFavAction',idCont:info[0]},"dummy_div");
    }
    else
    {
        $('#'+info[1]+'-'+info[0]).attr("class",fav);
        phpServer(getPath(info[1]),{type:'FavAction',idCont:info[0]},"dummy_div");
    }
    phpServer(getPath(info[1]),{type:'fav_rate_'+info[1],cont:info[1]},"table_"+info[1]+"_Fav_Rate_div");
});
//RATE CONTENT ACTION    
$(document).on('click', '.star-set', function(){
    var id3 = $(this).data("id3");
    var info = id3.split("%");
    var NoStar = this.id.split("-")[1];
    var emptyStar = "glyphicon glyphicon-star-empty star-set";
    var fullStar = "glyphicon glyphicon-star star-set";
    var limitEnd = Math.ceil(NoStar/5) * 5;
    var limitBgn = limitEnd - 5 + 1;
    var currentStar = NoStar - limitBgn + 1;
    for(var i = limitBgn; i<=limitEnd; i++){
        if(i<=NoStar){
            $('#star-'+i).attr("class", fullStar);
            if(i === parseInt(NoStar)){
                phpServer(
                    getPath(info[2]),
                    {
                        type:'RateAction',
                        idCont:info[0],
                        star:info[1]
                    },
                    "dummy_div");
                phpServer(
                    getPath(info[2]),
                    {
                        type:'fav_rate_'+info[2],
                        cont:info[2]
                    },
                    "table_"+info[2]+"_Fav_Rate_div");
            }
        }
        else{
            $('#star-'+i).attr("class", emptyStar);
        }
    }
    $('#rate-text-'+limitEnd).text(currentStar);
});
//FAV & RATE SHOW
$(document).on('click', '.FavRateContent', function(){
    var id0 = $(this).data("id0");
    //GRAPHICAL
    var status = $("#"+id0).css("text-decoration");
    if(status === 'none'){
        $("#"+id0).css("text-decoration","line-through");    
    }
    else{
        $("#"+id0).css("text-decoration","none");
    }
    //LOGICAL
    var info = id0.split("_");
    var favStatus = $("#fav_"+info[1]).css("text-decoration");
    var rateStatus = $("#rate_"+info[1]).css("text-decoration");
    if(favStatus === 'none' && rateStatus === 'none')
    {
        //both
        phpServer(getPath(info[1]),{type:'fav_rate_'+info[1],cont:info[1]},"table_"+info[1]+"_Fav_Rate_div");
    }
    else if(favStatus === 'line-through' && rateStatus === 'line-through')
    {
        //nothing
        $("#table_"+info[1]+"_Fav_Rate_div").text('');
    }
    else{
        //rates o favorites
        phpServer(getPath(info[1]),{type:id0,cont:info[1]},"table_"+info[1]+"_Fav_Rate_div");
    }
});