/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var nutriID = -1;
var indicadorID = -1;
var path = "../../Controller/php/User/OpsNutritionControl.php";

//------------------------- AJAX CALL ----------------------------------
function list_ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}

function getColors(exp){
    var color = '';
    switch(exp){
        case 'red':
            color = 'rgba(205,48,48,0.5)';
        break;
        case 'yellow':
            color = 'rgba(255,206,86,0.5)';
        break;
        case 'green':
            color = 'rgba(53,205,48,0.5)';
        break;
        case 'blue':
            color = 'rgba(87,160,255,0.5)';
        break;
    }
    return color;
}

function indicador_ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: true,
        async: true,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){
            var obj = JSON.parse(d);
            var lbl = new Array(obj.length-1);
            var dt = new Array(obj.length-1);
            var cl = new Array(obj.length-1);
            for(var j = 1; j < obj.length; j++){
                lbl[j-1] = obj[j][0];
                dt[j-1] = obj[j][1];
                cl[j-1] = getColors(obj[j][2]);
            }
            var ctx = document.getElementById("canvas_div");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: lbl,
                    datasets: [
                        {
                            label: 'Indicador',
                            data: dt,
                            backgroundColor: cl,
                            borderColor: 'rgba(220,220,220,1)',
                            borderWidth: 1
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'UDM'
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'Fecha'
                            }
                        }]
                    },
                    responsive: true
                }
            });
        }  
    });
}
//------------------------------- SHOW -----------------------------------------
function fetch_data()  
{ 
    list_ajax_call(path,{type:'Indicador_ddl'},"Indicadores_div");
    list_ajax_call(path,{type:'RatePro',pro:'Nutricionista'},"RateNutri_div");
}    
//------------------------- MESSAGE ALERT ----------------------------------
function messages(msg,clss){
        $('#myAlert').attr("class", clss);
        $('#myAlert').text(msg);
        $('#myAlert').show('slow');
        setTimeout ( function () {$('#myAlert').hide('slow');}, 2000);
    }
$(document).ready(function() {
    fetch_data();
    //------------------------------- DROP DOWN LIST -----------------------------------------
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL == 'coach'){
            nutriID = selected;
            list_ajax_call(path,{type:'ProfilePic',coachID:nutriID},"profilePic_div");
        }else if(nameDDL == 'ddl_indicador'){
            ddl_indicador = selected;
            list_ajax_call(path,{type:'graph'},"graph_div");
            indicador_ajax_call(path
                    ,{type:'Indicador_graph',idIndicador:ddl_indicador}
                    ,"canvas_div");
            list_ajax_call(path,{type:'Goal_bar',idIndicador:ddl_indicador},"goal_div");        
            list_ajax_call(path,{type:'goal_icon_div'},"goal_icon_div");        
            //messages('Selecciona un indicador',"alert alert-danger");
        }
    });
    //------------------------- SAVE RATE ---------------------------------
    $(document).on('click', '#saveRate', function(){
        var rateNo = $('#rate_text').text();
        if(nutriID > 0){
            if(rateNo == ' '){
                messages('Ingrese al menos la calificación',"alert alert-danger");
            }
            else{
                var comment = $('#comment').val();    
                list_ajax_call(path,
                                {type:'SaveRate',
                                proID:nutriID,
                                rateNo:rateNo,
                                comment:comment},
                                "dummy_div");
                //alert
                messages('¡Gracias! por tus valiosos comentarios.',"alert alert-info");
                //clean
                list_ajax_call(path,{type:'RatePro',pro:'Nutricionista'},"RateNutri_div");
                $('.selectpicker').selectpicker('show');
                nutriID = -1;
            }
        }
        else
            messages('Seleccione nutricionista',"alert alert-danger");
    });
});