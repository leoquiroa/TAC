var chocale = true;
var path = "../../Controller/php/User/OpsNotifications.php";
var topTenDate = -1;
var topTenPlace = -1;
var IsShow = false;

function phpServer(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init()  
{
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'TypeNoti'
        },  
        dataType:"text",  
        success:function(d){  
            if(d === 'Personal')
                phpServer(path,{type:'BellEvents'},"divEvents");
            else if(d === 'Calendar')
                phpServer(path,{type:'CalendarEvents'},"divEvents");
        }  
   });
}

$(document).ready(function() 
{
    init();
});
$(document).on('click', '.NotificationRow', function()
{   
    var info = $(this).data("id0").split('#');
    
    phpServer(path,
        {
            type:'ViewCalendarEvent'
            ,idNewObj:info[0]
            ,typeNoti:info[1]
        },"dummy_div");
});