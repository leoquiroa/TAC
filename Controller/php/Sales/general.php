<?php
    class generalSalesFunctions{
        public function getList($sql,$title,$idDll,$sqlOps,$idColumn,$idName){
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '<select class="selectpicker" data-style="btn-info" title="'.$title.'" data-width="100%" id="'.$idDll.'">';    
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    $list .= '<option value="'.$row[$idColumn].'">'.$row[$idName].'</option>';
                }
            }
            return $list .= '</select>';
        }
        public function get_current_measure($sql,$sqlOps){
            $data = new Data();
            $person = new Person();
            $result_diff = $sqlOps->sql_multiple_rows($sql);
            $first = 1;
            while($row = $result_diff->fetch_assoc())
            {
                if($first > 0){ //only the first time
                    $person->setAll($row["idPerson"],$row["namePerson"],$row["idData"],$row["nameIndicador"],$row["valueIndicador"],$row["Fecha"]);
                    $first = 0;
                    continue;
                }
                //------------------------------------------------------------------
                if($person->idPerson== $row["idPerson"]){                       //same person
                    if($person->idData == $row["idData"]){}                     //same data
                    else{                                                       //not same data
                        $data->setData($person);
                        $person = $this->storageObjectValues($person, $row, array('idData', 'nameIndicador'));
                    }
                }
                else{                                                           //not same person
                    $data->setData($person);
                    $person = $this->storageObjectValues($person, $row, array('idPerson', 'namePerson', 'idData', 'nameIndicador'));
                }
                $person = $this->storageObjectValues($person, $row, array('valueIndicador', 'Fecha'));
            }
            $data->setData($person);
            return $data->getData();
        }
        public function storageObjectValues(Person $person, $row, $attributes){
            foreach($attributes as $attribute) {
                $person->$attribute = $row[$attribute];
            }
            return $person;
        }
        public function filter_by_value($array, $index, $value){ 
            if(is_array($array) && count($array)>0)  
            { 
                foreach(array_keys($array) as $key){ 
                    $temp[$key] = $array[$key][$index]; 
                    if ($temp[$key] == $value){ 
                        $newarray[$key] = $array[$key]; 
                    } 
                } 
            } 
            return $newarray; 
        } 
        public function get_statistical_values($table, $nameIndicador, $value){
            $indicatorTable = $this->filter_by_value($table, $nameIndicador, $value);
            $x = 0;
            foreach ($indicatorTable as $key => $value) {
                $indicatorColumn[$x++] = $indicatorTable[$key]['valueIndicador'];
            }
            $stats = array(); //min, average, max
            $stats[] = array("min" => min($indicatorColumn)
                            ,"avg" => round(array_sum($indicatorColumn)/count($indicatorColumn),2)
                            ,"max" => max($indicatorColumn));
            return $stats;
        }
        public function getArrayMeasures($sql,$sqlOps){
            $table = $sqlOps->sql_multiple_rows($sql);
            $indicator = array(); //idPerson, idData, value
            while($row = $table->fetch_assoc()){
                $indicator[] = array(
                    "idPerson" => $row["idPerson"]
                    ,"idData" => $row["idData"]
                    ,"value" => $row["value"]
                );
            }
            return $indicator;
        }
        public function getSucces_Fail_Goal($Initial,$Current,$Goal){
            $achieved = 0;
            $fail = 0;
            if(count($Initial) && count($Current) && count($Goal)){
                foreach ($Initial as $key => $value) {
                    $diff = $Goal[$key]['value'] - $Initial[$key]['value'];
                    //positive
                    if($diff > 0){
                        $Current[$key]['value'] >= $Goal[$key]['value'] ? $achieved++ : $fail++;
                    }
                    //negative
                    else if($diff < 0){
                        $Current[$key]['value'] <= $Goal[$key]['value'] ? $achieved++ : $fail++;
                    }
                }
            }
            $result[] = array("success" => $achieved,"fail" => $fail);
            return $result;
        }
    }  
    class Data{
        public $data = array();
        public function setData(Person $person){
            $this->data[] = array(
                "idPerson" => $person->idPerson,                                //idPerson
                "namePerson" => $person->namePerson,                            //namePerson
                "idData" => $person->idData,                                    //idData
                "nameIndicador" => $person->nameIndicador,                      //nameIndicador
                "valueIndicador" => $person->valueIndicador,                    //valueIndicador
                "Fecha" => $person->Fecha                                       //Fecha
            );  
        }
        public function getData(){
            return $this->data;
        }
    }
    class Person {
        public $idPerson = -1;
        public $namePerson = '';
        public $idData = -1;
        public $nameIndicador = '';
        public $valueIndicador = -1;
        public $Fecha = '';
        public function setAll(
                $idPerson,
                $namePerson,
                $idData,
                $nameIndicador,
                $valueIndicador,
                $Fecha) 
        {
            $this->idPerson = $idPerson;
            $this->namePerson = $namePerson;
            $this->idData = $idData;
            $this->nameIndicador = $nameIndicador;
            $this->valueIndicador = $valueIndicador;
            $this->Fecha = $Fecha;
        }
    }
    class generalSalesVariables{
        public $range = 5;
        public $jumps = 8;
        public $low_age = 15;
        public $no_camps = 4;
    }    
?>