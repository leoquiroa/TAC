<?php
    ///////////////// VARIABLES /////////////////
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    ////////////////////////////////////////////////////////////////////////////
    // DB Model
    ////////////////////////////////////////////////////////////////////////////
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();    
    ////////////////////////////////////////////////////////////////////////////
    // General Functions & Variables
    ////////////////////////////////////////////////////////////////////////////
    include "../../php/Nutrition/general.php";
    $generalFn = new generalNutricionistFunctions();      
    ////////////////////////////////////////////////////////////////////////////////
    // HOME
    ////////////////////////////////////////////////////////////////////////////////
    switch ($type_data){
        case 'ddl_category':
            $output = $generalFn->getList(
                        "CALL sp_catalog_list('NombreDeClase')",
                        "Categoria",
                        "ddl_category",
                        $sqlOps); 
        break;
        case 'save_event':
            $sql = "CALL sp_event_add("
                    . "'".$_POST['title']."',"
                    . "'".$_POST['datetime']."',"
                    . "'".$_POST['place']."',"
                    . "'".$_POST['categoryId']."',"
                    . "'".$_POST['date_from']."',"
                    . "'".$_POST['date_to']."',@si)";
            $output = $sqlOps->sql_exec_op_return($sql);
        break;
        case 'update_event':
            $sql1 = "CALL sp_event_update("
                    . "'".$_POST['id']."',"
                    . "'".$_POST['column_name']."',"
                    . "'".$_POST['texto']."')";
            $sqlOps->sql_exec_op($sql1);
        break;
        case 'menuType':
            session_start();
            $output = $_SESSION['typePerson'];
        break;    
    }
    ////////////////////////////////////////////////////////////////////////////////
    // OUTPUT
    ////////////////////////////////////////////////////////////////////////////////
    echo $output == '' ? '' : $output;    
?>