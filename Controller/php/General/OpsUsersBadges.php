<?php
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    $today = date("Y-m-d");
    
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    
    include "../../php/User/general.php";
    $generalFn1 = new generalUserFunctions();
    $generalVar1 = new generalUserVariables();
    
    switch ($type_data){
        //menu
        case 'menuType':
            session_start();
            $output = $_SESSION['typePerson'];
        break;
        //show
        case 'listBadges':
            $sql2 = "CALL sp_badgesperperson_all('Images')";
            $result2 = $sqlOps->sql_multiple_rows($sql2);
            $count2 = $result2 ? mysqli_num_rows($result2) : -1;
            $arrayBadges = array();
            if($count2 > 0) {
                $ix = 0;
                while($row2 = $result2->fetch_assoc()){
                    $arrayBadges[$ix][0] = $row2["idPerson"];
                    $arrayBadges[$ix][1] = $row2["name"];
                    $arrayBadges[$ix][2] = $row2["image"];
                    $ix++;
                }
            }
            $sql1 = "CALL sp_badgesperperson_all('List')";
            $result1 = $sqlOps->sql_multiple_rows($sql1);
            $count = $result1 ? mysqli_num_rows($result1) : -1;
            if($count > 0) {
                $list = '';
                while($row1 = $result1->fetch_assoc()){
                    $listImages = '';
                    $list .= '
                    <tr>
                        <td width="20%">'.$row1["Place"].'</td>
                        <td width="20%">'.$row1["namePerson"].'</td>
                        <td width="50%">';
                    $i = 0 ;
                    
                    do{
                        if($row1["idPerson"]==$arrayBadges[$i][0])
                        {
                            $listImages .= '<img src="../../Multimedia/img/badges/'.$arrayBadges[$i][2].'" title="'.$arrayBadges[$i][1].'" style="width:40px;height:40px;">';
                        }
                        $i++;
                    }while($i < sizeof($arrayBadges));
                    
                    $list .= $listImages.'
                        </td>
                        <td width="10%">'.$row1["Total"].'</td>
                    </tr>';
                }
                $output = '
                <table class="table table-hover table-bordered" style="font-size: 13px;">
                    <thead>
                      <tr>
                        <th>Lugar</th>
                        <th>Nombre</th>
                        <th>Medallas</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody style="font-size:10px;">';
                $output .= $list.'
                    </tbody>
                </table>';
            }
        break;
    }
    echo $output == '' ? '' : $output;