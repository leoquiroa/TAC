<?php
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    include "../../php/User/general.php";
    $generalFn1 = new generalUserFunctions();
    $generalVar1 = new generalUserVariables();
    include "../../php/Nutrition/general.php";
    $generalFn2 = new generalNutricionistFunctions();
    $generalVar2 = new generalNutricionistVariables();      
    
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    $today = date("Y-m-d");
    
    switch ($type_data){
        //menu
        case 'menuType':
            session_start();
            $output = $_SESSION['typePerson'];
        break;
        //show
        case 'all_images':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql = "CALL sp_person_picture('".$idPatient."','ALL')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    if($row["active"] == 1){
                        $output .= '<img src="../../Multimedia/img/people/'.$row["url"].'" class="mainImage" alt=""/>
                                   <br/>
                                   <label>'.$row["DatePic"].'</label>
                                   <br/>';
                    }
                    else{
                        $output .= '<img src="../../Multimedia/img/people/'.$row["url"].'" class="miniatureImage" alt=""/>&nbsp;';
                    }
                }
            }
            
        break;
        case 'showInformation':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql = "CALL sp_administrator_people_top('".$idPatient."')";
            $result = $sqlOps->sql_single_row($sql);
            if(!is_null ($result["Nombre"])){
                $output .= '
                    <p class="pNameUser">'.$result["Nombre"].'</p>
                    <p class="pNick">'.$result["Nick"].'</p>
                    <p class="pPlace">'.$result["namePlace"].'</p>
                    <span class="label"> Meta peso : '.$result["Peso"].' lbs</span>
                    <span class="label"> Meta grasa : '.$result["GrasaCorporal"].' %</span>
                    <span class="label"> IMC : '.$result["IMC"].'</span>
                    <span class="label"> Talla: 36</span>
                    <span class="label"> Edad : '.$result["Edad"].'</span>';
            }
        break;
        case 'showBars':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $Points = $generalFn1->fn_getBars($idPatient,'Ordinario',$today,$sqlOps);
            $sessionPoints = $Points[0];
            $monthPoints = $Points[1];
            $Miles = $generalFn1->fn_getBars($idPatient,'Extraordinario',$today,$sqlOps);
            $sessionMiles = $Miles[0];
            $monthMiles = $Miles[1];

            $percentageSessionPoints = round(($sessionPoints/$generalVar1->maxPointsSession)*100,2);
            $percentageSessionMiles = round(($sessionMiles/$generalVar1->maxMilesSession)*100,2);
            $percentageMonthlyPoints = round(($monthPoints/$generalVar1->maxPointsMonthly)*100,2);
            $percentageMonthlyMiles = round(($monthMiles/$generalVar1->maxMilesMonthly)*100,2);

            $position = 0;
            $idPlacePatient = isset($_SESSION['idPlacePatient']) ? $_SESSION['idPlacePatient'] : -1;
            $sql = "CALL sp_person_monthly_rank('".$today."','".$idPlacePatient."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    if($row["idPerson"] == $idPlacePatient){
                        $position = $row["position"];
                        break;
                    }
                }
            }
            $percentagePosition = round(($position/$count)*100,2);

            $output .= ' 
                <div class="row">
                    <div class="col-md-6">
                        <p class="pIndicators">Temporada</p>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom1" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxPointsSession.'" style="width: '.$percentageSessionPoints.'%">
                                <span>PO: '.$sessionPoints.'</span>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom2" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxMilesSession.'" style="width: '.$percentageSessionMiles.'%">
                                <span>ME: '.$sessionMiles.'</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p class="pIndicators">Mes</p>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom3" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxPointsMonthly.'" style="width: '.$percentageMonthlyPoints.'%">
                                <span>PO: '.$percentageMonthlyPoints.'</span>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom2" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxMilesMonthly.'" style="width: '.$percentageMonthlyMiles.'%">
                                <span>ME: '.$monthMiles.'</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="progress">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuemin="0" aria-valuemax="'.$count.'" style="width: '.$percentagePosition.'%">
                        <span>Posición: '.$position.' de '.$count.'</span>
                    </div>
                </div>';
        break;
        //table
        case 'indicators_ddl':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql = "CALL sp_nutriappoint_past_list('".$idPatient."')";  
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $output = '<select class="selectpicker" data-style="btn-info" data-width="100%" title="'.$_POST['title'].'" id="ddl_indicators">';
                while($row = $result->fetch_assoc())
                {  
                    $output .= '<option data-subtext="-  '.$row["MeasureType"].'" value="'.$row["idNutriAppoint"].'#'.$row["MeasureType"].'">'.$row["Fecha"].'</option>';
                }
                $output .= '</select>';
            }
        break;
        case 'indicators_past_info':
            $sql = "CALL sp_nutriappoint_past_general('".$_POST['idNutriAppoint']."')";  
            $row = $sqlOps->sql_single_row($sql);
            if($row !== ''){
                $output = '
                <table class="table table-hover table-bordered table-condensed">
                    <thead>
                      <tr>
                        <th>Campo</th>
                        <th>Valor</th>
                      </tr>
                    </thead>
                    <tbody style="font-size:10px;">';
                $list = ''; $ix = 0;
                foreach ($row as $field){
                    $listName = $generalVar2->NutriInfo[$ix];
                    if($listName == 'Puntos'){
                        $list .= '
                        <tr>
                            <td>'.$listName.'</td>
                            <td class="assignPoints" 
                                contenteditable="true" 
                                data-id0="'.$_POST['idNutriAppoint'].'" 
                                id="update'.$listName.'">'.$field.'</td>
                        </tr>';
                    }
                    else if($listName == 'Fecha'){
                        $list .= '
                        <tr>
                            <td>'.$listName.'</td>
                            <td>
                                <input 
                                    style="height: 12px; font-size:10px;" 
                                    type="text" 
                                    class="form-control changeDate" 
                                    value="'.$field.'" 
                                    id="update'.$listName.'">
                            </td>
                        </tr>';
                    }
                    else if($listName == 'Hora'){
                        $list .= '
                        <tr>
                            <td>'.$listName.'</td>
                            <td>
                                <input 
                                    style="height: 12px; font-size:10px;" 
                                    type="text" 
                                    class="form-control changeHour" 
                                    value="'.$field.'" 
                                    id="update'.$listName.'">
                            </td>
                        </tr>';
                    }
                    else{
                        $list .= '
                        <tr>
                            <td>'.$listName.'</td>
                            <td>'.$field.'</td>
                        </tr>';
                    }
                    $ix++;
                }
                $output .= $list.'
                    </tbody>
                </table>';
            }
        break;
        case 'indicators_table':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql1 = "CALL sp_detailsnutriappoint_list(".$idPatient.",".$_POST['idNutriAppoint'].",'Ranges')";
            $result = $sqlOps->sql_multiple_rows($sql1);
            $list = '';
            $initialValue = '';
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $colorInitial = '';$colorCurrent = '';$colorGoal = '';
                while($row = $result->fetch_assoc())
                {  
                    $glyph = (intval($row["Meta"]) > intval($row["Inicial"])) ? 'up' : 'down';
                    $color = (intval($row["Meta"]) > intval($row["Inicial"])) ? 'green' : 'red';
                    switch($_POST['measureType']){
                        case 'inicial':
                            $colorInitial = 'blue';$colorCurrent = 'gray';$colorGoal = 'gray';
                            $list .= '
                            <tr>
                                <td style="cursor: pointer;" data-id1="'.$row["idIndicador"].'" class="indicatorSee">'
                                    . '<i class="fa fa-chevron-circle-'.$glyph.'" aria-hidden="true" style="color:'.$color.';"></i> '
                                    .$row["nameIndicador"].
                                '</td>
                                <td contenteditable="true" class="indicatorUpdate" data-id0="'.$row["idIndicador"].'" id="color_'.$row["idIndicador"].'">'.$row["Inicial"].'</td>
                                <td></td>
                                <td></td>
                            </tr>';
                        break;
                        case 'actual':
                            $colorInitial = 'gray';$colorCurrent = 'blue';$colorGoal = 'gray';
                            $list .= '
                            <tr>
                                <td style="cursor: pointer;" data-id1="'.$row["idIndicador"].'" class="indicatorSee">'
                                    . '<i class="fa fa-chevron-circle-'.$glyph.'" aria-hidden="true" style="color:'.$color.';"></i> '
                                    .$row["nameIndicador"].
                                '</td>
                                <td>'.$row["Inicial"].'</td>
                                <td contenteditable="true" class="indicatorUpdate" data-id0="'.$row["idIndicador"].'" id="color_'.$row["idIndicador"].'">'.$row["Actual"].'</td>
                                <td>'.$row["Meta"].'</td>
                            </tr>';
                        break;
                        case 'meta':
                            $colorInitial = 'gray';$colorCurrent = 'gray';$colorGoal = 'blue';
                            $list .= '
                            <tr>
                                <td style="cursor: pointer;" data-id1="'.$row["idIndicador"].'" class="indicatorSee">'
                                    . '<i class="fa fa-chevron-circle-'.$glyph.'" aria-hidden="true" style="color:'.$color.';"></i> '
                                    .$row["nameIndicador"].
                                '</td>
                                <td></td>
                                <td></td>
                                <td contenteditable="true" class="indicatorUpdate" data-id0="'.$row["idIndicador"].'" id="color_'.$row["idIndicador"].'">'.$row["Meta"].'</td>
                            </tr>';
                        break;
                    }
                }
                $output = '
                <table class="table table-hover table-bordered" style="font-size: 13px;">
                    <thead>
                      <tr>
                        <th>Indicador</th>
                        <th style="color:'.$colorInitial.'">Inicial</th>
                        <th style="color:'.$colorCurrent.'">Actual</th>
                        <th style="color:'.$colorGoal.'">Meta</th>
                      </tr>
                    </thead>
                    <tbody style="font-size:10px;">';
                $output .= $list.'
                    </tbody>
                </table>';
            }            
        break;
        case 'edit_measure':
            $sql1 = "CALL sp_detailsnutriappoint_exist(".$_POST['idNutriAppoint'].",".$_POST['idData'].")";
            $exist = $sqlOps->sql_single_row($sql1);
            $sql2 = "CALL sp_detailsnutriappoint_edit(".
                        $exist["idDetails"].",'".trim(str_replace(' ','',$_POST['dataValue']), "\n")."')";
            $output = $sqlOps->sql_exec_op($sql2);
        break;
        case 'assign_point':
            $sql2 = "CALL sp_nutriappoint_assign_points('".$_POST['idNutriAppoint']."','".$_POST['dataValue']."')";
            $output = $sqlOps->sql_exec_op($sql2);            
        break; 
        case 'assign_date_points':
            $sql3 = "CALL sp_nutriappoint_update_date_points("
                . "'".$_POST['idNutriAppoint']."'"
                . ",'".$_POST['date']."'"
                . ",'".$_POST['hour']."'"
                . ",'".$_POST['points']."')";
            $output = $sqlOps->sql_exec_op($sql3);            
        break; 
        //graph
        case 'refresh_canvas':
            $output = '<canvas id="canvas-line"></canvas>';            
        break;
        case 'lectures':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $res = array();
            $res[0][0] = "Tiempo";
            $res[0][1] = "Valor";
            $res[0][2] = "Name";
            $sql1 = "CALL sp_person_nutrition_list('".$idPatient."','".$_POST['idIndicator']."','Details')";
            $result = $sqlOps->sql_multiple_rows($sql1);
            $ix = 0;
            while($row = $result->fetch_assoc())
            {
                $res[$ix][0] = $row["Tiempo"];
                $res[$ix][1] = $row["Valor"];
                $res[$ix][2] = $row["NameIndicator"];
                $ix++;
            }
            echo json_encode($res);            
        break;
        //watches
        case 'tanita':
            session_start();
            $idPerson = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $res = array();
            $ix = 0;

            $sql1 = "CALL sp_person_nutrition_list('".$idPerson."',getCategoryId('Indicadores','Porcentaje De Agua'),'Last')";
            $indicator1 = $sqlOps->sql_single_row($sql1);
            if($indicator1 !== ''){
                $res[$ix][0] = $indicator1["Tiempo"];
                $res[$ix][1] = $indicator1["Valor"];
                $ix++;
            }
            
            $sql2 = "CALL sp_person_nutrition_list('".$idPerson."',getCategoryId('Indicadores','Porcentaje De Grasa Corporal'),'Last')";
            $indicator2 = $sqlOps->sql_single_row($sql2);
            if($indicator2 !== ''){
                $res[$ix][0] = $indicator2["Tiempo"];
                $res[$ix][1] = $indicator2["Valor"];
                $ix++;
            }

            $sql3 = "CALL sp_person_nutrition_list('".$idPerson."',getCategoryId('Indicadores','Indice de Masa Corporal - IMC'),'Last')";
            $indicator3 = $sqlOps->sql_single_row($sql3);
            if($indicator3 !== ''){
                $res[$ix][0] = $indicator3["Tiempo"];
                $res[$ix][1] = $indicator3["Valor"];
                $ix++;
            }

            $sql4 = "CALL sp_person_nutrition_list('".$idPerson."',getCategoryId('Indicadores','Porcentaje Grasa Visceral'),'Last')";
            $indicator4 = $sqlOps->sql_single_row($sql4);
            if($indicator4 !== ''){
                $res[$ix][0] = $indicator4["Tiempo"];
                $res[$ix][1] = $indicator4["Valor"];
                $ix++;
            }

            echo count($res) > 0 ? json_encode($res) : '';
        break;    
        //new appoint
        case 'appoint_info':
            $sql = "CALL sp_catalog_list('TiposDeMedicionEnCitaNutricional')";  
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $output = '<select class="selectpicker" data-style="btn-info" data-width="100%" title="Tipo de medición" id="ddl_measure_type">';
                while($row = $result->fetch_assoc())
                {  
                    $output .= '<option value="'.$row["idCatalog"].'#'.$row["nameCatalog"].'">'.$row["nameCatalog"].'</option>';
                }
                $output .= '</select>';
            }
        break;
        case 'add_same_day_appoint':
            //retrieve data of the Nutri and Person
            session_start();
            $idNutri = isset($_SESSION['idPerson']) ? $_SESSION['idPerson'] : -1;
            $idPerson = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            //return of the Session
            $sql = "CALL sp_person_session('".$idPerson."')";  
            $resultSession = $sqlOps->sql_single_row($sql);
            //return of the PointType
            $sql = "CALL sp_nutriAppoint_check('".$idPerson."')";  
            $resultPointType = $sqlOps->sql_single_row($sql);
            //save of the appointment
            $sql4 = "CALL sp_nutriappoint_add("
                .$resultSession["idSession"]
                .",".$idNutri
                . ",getPlaceId(".$idNutri.")"
                . ",'".date("Y-m-d g:i")."'"
                . ",300"
                . ",getCategoryId('TipoDeCitaNutricional','El mismo dia')"
                . ",".$_POST['newIdMeasureType']
                . ",".$resultPointType["PointType"]
                . ",@si)";
            $idAppoint = $sqlOps->sql_exec_op_return($sql4);
            //set all the indicators
            $list = $sqlOps->sql_multiple_rows("CALL sp_catalog_list('Indicadores')");
            $count = $list ? mysqli_num_rows($list) : -1;
            if($count > 0){
                while($row = $list->fetch_assoc()){
                    $sql5 = "CALL sp_detailsnutriappoint_add(".$idAppoint.",".$row["idCatalog"].",NULL)";
                    $sqlOps->sql_exec_op($sql5);
                }
            }
            $output = $idAppoint;
        break;
        case 'titleNewAppoint':
            $sql = "CALL sp_NutriAppoint_Date('".$_POST['idNewAppoint']."')";  
            $result = $sqlOps->sql_single_row($sql);
            $output = $result["Fecha"];
        break;
    }
    echo $output == '' ? '' : $output;