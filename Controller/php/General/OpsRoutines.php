<?php
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();    
    include "../../php/Nutrition/general.php";
    $generalFn = new generalNutricionistFunctions();      
    
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    switch ($type_data){
        case 'ddl_category':
            $output = $generalFn->getList(
                        "CALL sp_catalog_list('NivelesDeEjercicios')",
                        "Nivel de Ejercicio",
                        "ddl_category",
                        $sqlOps); 
        break;    
        case 'ddl_place':
            $output = $generalFn->getList(
                        "CALL sp_catalog_list('Lugar')",
                        "Sede",
                        "ddl_place",
                        $sqlOps); 
        break;
        case 'users_table_venue':
            $list_people = '';
            $sql = "CALL sp_administrator_people_list_by_place('','".$_POST['placeID']."','ALL')";  
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                while($row = $result->fetch_assoc())
                {  
                    $list_people .= '
                    <tr>
                        <td><input type="checkbox" class="who" value="'.$row["idPerson"].'"></td>
                        <td>'.$row["namePerson"].'</td>
                    </tr>';
                }
                $output .= '
                    <table class="table scroll table-condensed table-hover table-bordered">
                        <thead>
                            <tr>
                                <th><input type="checkbox" class="who" value="0" id="mainCheck"></th>
                                <th>Nombre</th>
                            </tr>
                        </thead>
                        <tbody>';
                $output .= $list_people;
                $output .= '
                        </tbody>
                    </table>';
            }
        break;
        case 'upload_video':
            $sql0 = "CALL sp_catalog_id('Contenido','Videos')";
            $idVideo = $sqlOps->sql_single_row($sql0);

            $path = split("=",$_POST['link']);
            $sql = "CALL sp_digitalcontent_add("
                    . "'".$_POST['title']."',"
                    . "'".$path[1]."',"
                    . "'".$path[1]."',"
                    . "'".$idVideo["idCatalog"]."',"
                    . "'".$_POST['categoryId']."',"
                    . "'".$_POST['date_from']."',"
                    . "'".$_POST['date_to']."',@si)";
            $idCont = $sqlOps->sql_exec_op_return($sql);
            session_start();
            $idNutri = $_SESSION['idPerson'];
            if($_POST['allPlaces']=='true'){
                $sql = "CALL sp_person_getAll()";  
                $result = $sqlOps->sql_multiple_rows($sql);
                $count = $result ? mysqli_num_rows($result) : -1;
                if($count > 0) 
                {
                    while($row = $result->fetch_assoc())
                    {
                        $sql_cont_per = "CALL sp_digitalcontent_per_person('".$idCont."','".$row["idPerson"]."',".$idNutri.")";
                        $sqlOps->sql_exec_op($sql_cont_per);
                    }
                }
            }
            else
            {
                $people = $_POST['Going'];
                foreach ($people as $person) 
                {
                    $sql_cont_per = "CALL sp_digitalcontent_per_person('".$idCont."','".$person."',".$idNutri.")";
                    $sqlOps->sql_exec_op($sql_cont_per);
                }
            }
            $output = $idCont;
        break;    
        //menu
        case 'menuType':
            session_start();
            $output = $_SESSION['typePerson'];
        break;        
    }
    echo $output == '' ? '' : $output;    