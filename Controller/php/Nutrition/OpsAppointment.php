<?php
    include "../../../Model/SqlOperations.php";
    include "../../php/Nutrition/general.php";
    session_start();
    
    $output = '';
    $idNutricionist = $_SESSION['idPerson'];
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    $sqlOps = new SqlOperations();   
    $generalFn = new generalNutricionistFunctions();   
    $generalVar = new generalNutricionistVariables();   
    
    switch ($type_data){
        //begin
        case 'Calendar':
            $sql = "CALL sp_nutriappoint_dates_list('','".$idNutricionist."','Month')";
            $res = $generalFn->get_array_calendar_event($sql,$sqlOps);
            echo json_encode($res);
        break;  
        //select one day
        case 'CalendarDetails':
            $output .= '
                <table class="table scroll table-condensed table-hover table-bordered" id="tableAppoint" style="font-size:12px;">
                    <thead>
                        <tr>
                            <th>Hora</th>
                            <th>Lugar</th>
                            <th>Nombre</th>
                            <th>Tipo de Cita</th>
                            <th>Tipo de Medición</th>
                        </tr>
                    </thead>';
            $sql = "CALL sp_nutriappoint_dates_list('".$_POST['fecha']."','".$idNutricionist."','Day')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $list_people = '';
                while($row = $result->fetch_assoc()){
                    $list_people .= '
                    <tr>
                        <td>'.$row["Hora"].'</td>
                        <td>'.$row["Place"].'</td>
                        <td class="edit_who" data-id1="'.$row["idNutriAppoint"].'" style="cursor: pointer;">'.$row["Nombre"].'</td>
                        <td>'.$row["TipoDeCita"].'</td>
                        <td>'.$row["TipoDeMedicion"].'</td>
                    </tr>';
                }
                $output .= '<tbody>'.$list_people.'</tbody>';
            }
            $output .= '</table>';            
        break;  
        case 'DayInLetters':
            $output = '<h5>'.$generalVar->dias[$_POST['nameDay']].'</h5>
                <h2>'.$_POST['numberDay'].'</h2>
                <h5>'.$generalVar->meses[$_POST['nameMonth']].', '.$_POST['nameYear'].'</h5>';            
        break;  
        case 'place_ddl':
            $output = $generalFn->getList(
                        "CALL sp_catalog_list('Lugar')",
                        $_POST['title'],
                        "ddl_place",
                        $sqlOps);           
        break;      
        case 'people_ddl':
            $list_people = '';
            $sql = "CALL sp_administrator_people_list_by_place('','".$_POST['placeID']."','ALL')";  
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $output = '<select class="selectpicker" data-style="btn-info" data-width="100%" title="Nombre" id="ddl_people" data-live-search="true" data-size="5">';
                while($row = $result->fetch_assoc())
                {  
                    $output .= '<option value="'.$row["idPerson"].'">'.$row["namePerson"].'</option>';
                }
                $output .= '</select>';
            }            
        break;    
        case 'measurement_ddl':
            $output = $generalFn->getList(
                        "CALL sp_catalog_list('TiposDeMedicionEnCitaNutricional')",
                        $_POST['title'],
                        "ddl_measurement",
                        $sqlOps);      
        break;  
        //Save
        case 'SaveAppointment':
            //get session
            $sql1 = "CALL sp_person_session('".$_POST['idPerson']."')";
            $idSession = $sqlOps->sql_single_row($sql1);  
            //number of appointments on the month
            $sql2 = "CALL sp_person_nutriAppointNumber_per_month(".$idSession['idSession'].",'".$_POST['date_appoint']."')";
            $NoDateMonth = $sqlOps->sql_single_row($sql2);
            $sql3 = "";
            if(intval($NoDateMonth['Number']) == 0){
                $sql3 = "CALL sp_catalog_id('AcumulacionDePuntos','Ordinario')";
            }
            else if (intval($NoDateMonth['Number']) > 0){
                $sql3 = "CALL sp_catalog_id('AcumulacionDePuntos','Extraordinario')";
            }
            $type = $sqlOps->sql_single_row($sql3);
            //save of the appointment
            $sql4 = "CALL sp_nutriappoint_add("
                    .$idSession['idSession']
                    .",'".$idNutricionist."'"
                    . ",'".$_POST['idPlace']."'"
                    . ",'".$_POST['date_appoint']." ".$_POST['hour_appoint']."'"
                    . ",NULL"
                    . ",getCategoryId('TipoDeCitaNutricional','Solicitada')"
                    . ",'".$_POST['measureType']."'"
                    . ",".$type['idCatalog'].""
                    . ",@si)";
            $idAppoint = $sqlOps->sql_exec_op_return($sql4);
            //set all the indicators
            $list = $sqlOps->sql_multiple_rows("CALL sp_catalog_list('Indicadores')");
            $count = $list ? mysqli_num_rows($list) : -1;
            if($count > 0){
                while($row = $list->fetch_assoc()){
                    $sql5 = "CALL sp_detailsnutriappoint_add(".$idAppoint.",".$row["idCatalog"].",NULL)";
                    $output = $sqlOps->sql_exec_op($sql5);
                }
            }
        break;  
        //delete
        case 'DeleteNutriAppointment':
            $sql1 = "CALL sp_detailsnutriappoint_delete(".$_POST['idNutriAppoint'].")";
            $details = $sqlOps->sql_exec_op($sql1);
            $sql2 = "CALL sp_nutriappoint_delete(".$_POST['idNutriAppoint'].")";
            $base = $sqlOps->sql_exec_op($sql2);
            $output = $details && $base;
        break;  
        //update
        case 'UpdateAppointment':
            $sql1 = "CALL sp_nutriappoint_update(".
                $_POST['idNutriAppoint'].", ".
                $_POST['idPlace'].", '".
                $_POST['dt_appoint']."', ".
                "getCategoryId('TipoDeCitaNutricional','Solicitada'), ".
                $_POST['measureType'].")";
            $output = $sqlOps->sql_exec_op($sql1);            
        break;      
        case 'ListAppointment':
            $sql = "CALL sp_nutriappoint_list(".$_POST['idNutriAppoint'].")";
            $row = $sqlOps->sql_single_row($sql);
            echo json_encode($row);            
        break;          
    }
    echo $output == '' ? '' : $output;   