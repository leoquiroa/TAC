<?php
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    include "../../php/Nutrition/general.php";
    $generalFn = new generalNutricionistFunctions();    
    switch ($type_data){
        //ddl
        case 'ddl_category':
            $output = $generalFn->getList(
                        "CALL sp_catalog_list('CategoriaDeFrase')",
                        $_POST['title'],
                        $_POST['id'],
                        $sqlOps); 
        break;
        //new
        case 'new_phrase':
            $sql2 = "CALL sp_phrase_add('".
                $_POST['txt_title']."','".
                $_POST['comment']."','".
                $_POST['categoryId']."','".
                $_POST['txt_date_from']."','".
                $_POST['txt_date_to']."',".
                "@si)";
            $output = $sqlOps->sql_exec_op_return($sql2);            
        break;
        //existent
        case 'existent_phrase':
            $output .= '
                <table class="table scroll table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="10%"></th>
                            <th width="30%">Titulo</th>
                            <th width="60%">Contenido</th>
                        </tr>
                    </thead>';
            $sql = "CALL sp_phrase_list('List','".$_POST['content_category']."',NULL)";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $list_phrase = '';
                while($row = $result->fetch_assoc()){
                    $list_phrase .= '
                    <tr>
                        <td width="10%"><input type="checkbox" class="which" value="'.$row["idPhrase"].'"></td>
                        <td width="30%">'.$row["titlePhrase"].'</td>
                        <td width="60%">'.$row["textPhrase"].'</td>
                    </tr>';
                }
                $output .= '<tbody style="font-size:10px">'.$list_phrase.'</tbody>';
            }
            $output .= '</table>';            
        break;    
        case 'pick_phrase':
            $sql = "CALL sp_phrase_list('Specific',NULL,'".$_POST['idCont']."')";
            $result = $sqlOps->sql_single_row($sql);
            echo json_encode($result);            
        break;
        //random
        case 'random':
            $sql1 = "CALL sp_phrase_list('Random',NULL,NULL)";
            $result = $sqlOps->sql_multiple_rows($sql1);
            $list = array();
            while($row = $result->fetch_assoc()){
                $list[] = $row["idPhrase"];
            }
            $inx = rand(0, count($list)-1);
            $sql2 = "CALL sp_phrase_list('Specific',NULL,'".$list[$inx]."')";
            $result2 = $sqlOps->sql_single_row($sql2);
            echo json_encode($result2);            
        break;    
    }
    echo $output == '' ? '' : $output;