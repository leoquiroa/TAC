<?php
    ///////////////// VARIABLES /////////////////
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    ////////////////////////////////////////////////////////////////////////////
    // DB Model
    ////////////////////////////////////////////////////////////////////////////
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();  
    ////////////////////////////////////////////////////////////////////////////
    // General Functions & Variables
    ////////////////////////////////////////////////////////////////////////////
    include "../../php/Administration/general.php";
    $generalFn = new generalAdministratorFunctions();        
    ////////////////////////////////////////////////////////////////////////////////
    // HOME
    ////////////////////////////////////////////////////////////////////////////////
    switch ($type_data){
        case 'dll_rol_type':
            $output = $generalFn->getList(
                    "CALL sp_catalog_list('RolDeUsuario')",
                    "Rol",
                    "ddl_rol_type",
                    $sqlOps,
                    "nameCatalog",
                    "nameCatalog",
                    array("Integrante","Administrador"));
        break;
        case 'dll_rol_list':
            $output = $generalFn->getList(
                    "CALL sp_professional_list('".$_POST['rol']."')",
                    "Profesional",
                    "ddl_rol_list",
                    $sqlOps,
                    "idPerson",
                    "namePerson",
                    array());           
        break;    
        //
        case 'pro_details':
            $sql = "CALL sp_administrator_people_details('".$_POST['userID']."','professional')";
            $lbl = array("Nombre", "Fecha de Nac.", "Altura", "Genero", "Telefono", "Correo", "Foto");
            $result = $sqlOps->sql_single_row($sql);
            $list = '';
            $path_pic = '';
            for($i = 0; $i<count($lbl); $i++){
                $editable = 'true';
                if($lbl[$i] == 'Genero'){
                    $editable = 'false';                    
                    $list_diff = $generalFn->getList(
                        "CALL sp_catalog_list('Genero')",
                        $result[$lbl[$i]],
                        "ddl_".$lbl[$i]."_update",
                        $sqlOps,
                        "idCatalog",
                        "nameCatalog",
                        array());
                    $list .= '
                        <tr>
                            <td>'.$lbl[$i].'</td>
                            <td contenteditable="'.$editable.'">'.$list_diff.'</td>
                        </tr>';
                }
                elseif($lbl[$i] == "Foto"){
                    $path_pic = $result[$lbl[$i]];
                }
                else{
                    $id_row = '';
                    if($lbl[$i] == "Nombre"){
                        $id_row .= 'name';}
                    elseif($lbl[$i] == "Fecha de Nac."){
                        $id_row .= 'birthday';}
                    elseif($lbl[$i] == "Altura"){
                        $id_row .= 'height';}
                    elseif($lbl[$i] == "Telefono"){
                        $id_row .= 'telephone';}
                    elseif($lbl[$i] == "Correo"){
                        $id_row .= 'mail';}
                    $list .= '
                        <tr>
                            <td >'.$lbl[$i].'</td>
                            <td contenteditable="'.$editable.'" class="class_'.$id_row.'" data-id_'.$id_row.'="'.$result["idPerson"].'" id="'.$result["idPerson"].''.substr($id_row,0,2).'">'.$result[$lbl[$i]].'</td>
                        </tr>';
                }
            }
            $output .= '
                    <div class="row">
                        <div class="col-md-7">
                            <table class="table table-condensed table-hover table-bordered">
                                <thead>
                                  <tr>
                                    <th>Campo</th>
                                    <th>Valor</th>
                                  </tr>
                                </thead>
                                <tbody>';
            $output .= $list;
            $output .= '
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-5">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width:230px; height:220px">
                                    <img src="../../Multimedia/img/people/'.$path_pic.'" alt="..." id="pp_preview">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 230px; max-height: 220px;"></div>
                                <div>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Cambiar</span>
                                    <span class="fileinput-exists">¿Otra?</span><input type="file" name="..."></span>
                                    <button type="button" class="btn btn-default fileinput-exists" id="update_profile_pic" data-id_npp="'.$result["idPerson"].'">Esta si</button>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Cancelar</a>
                                </div>
                            </div>
                        </div>
                    </div>';            
        break;
        //
        case 'dll_pro_time':
            $output = $generalFn->getList(
                    "CALL sp_catalog_list('Tiempo')",
                    "Tiempo",
                    "ddl_pro_time",
                    $sqlOps,
                    "nameCatalog",
                    "nameCatalog",
                    array("Semana"));                      
        break;
        case 'data_pro':
            $sql = "CALL sp_professional_statistics('".$_POST['idPerson']."','".$_POST['dateBgn']."','".$_POST['dateEnd']."','".$_POST['type_date']."','".$_POST['type_pro']."')";  
            
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $res = array();
                $ix = 1;
                $name = $_POST['type_pro'] == 'Entrenador' ? 'Clase' : 'Cita';
                while($row = $result->fetch_assoc())
                {  
                    $res[$ix][0] = $row[$name];
                    $res[$ix][1] = $row["Total"];
                    $ix++;
                }
                echo json_encode($res);
            }
        break;    
        case 'pro_table':
            $data = '';
            $obj = $_POST['obj'];
            for($a=1;$a<=sizeof($obj);){
                $data .= '
                    <tr>
                        <td>'.$obj[$a][0].'</td>
                        <td>'.$obj[$a][1].'</td>
                    </tr>';
                $a++;
            }

            $name = $_POST['type_pro'] == 'Entrenador' ? 'Clase' : 'Cita';
            $output .= '
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>'.$name.'</th>
                        <th>Cantidad</th>
                    </tr>
                </thead>
                <tbody>';
            $output .= $data;
            $output .= '
                </tbody>
            </table>';
        break;    
        case 'chart_pro':
            $output .= '<canvas id="chart-area"></canvas>';
        break;            
    }
    ////////////////////////////////////////////////////////////////////////////
    // OUTPUT
    ////////////////////////////////////////////////////////////////////////////
    echo $output == '' ? '' : $output;
?>