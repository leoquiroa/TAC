<?php
    session_start();
    $idUser = $_SESSION['idPerson'];
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    include "../../php/User/general.php";
    $generalFn = new generalUserFunctions();
    
    switch ($type_data)
    {
        //initial show Diets
        case 'signs_fav_rate_diet':
            $output = $generalFn->FavRateSigns("Mis dietas","Dietas");
        break;
        case 'table_diet': 
            $output = $generalFn->TableContent(
                    'Dietas',
                    NULL,
                    $idUser,
                    14,
                    'dietas',
                    'dietScroll',
                    1,
                    $sqlOps);
        break;
        //initial show Effort
        case 'signs_fav_rate_effort':
            $output = $generalFn->FavRateSigns("Mis pruebas","Esfuerzo");
        break;
        case 'table_effort':
            $noContent = $sqlOps->sql_single_row(
                    "CALL sp_digitalcontent_count('Dietas',NULL,'.$idUser.')");
            $output = $generalFn->TableContent(
                    'Esfuerzo',
                    NULL,
                    $idUser,
                    14,
                    'pruebas de esfuerzo',
                    'effortScroll',
                    (int)($noContent['TOTAL']*5)+1,
                    $sqlOps);
        break;
        //show Favorites/Rate in Diets
        case 'fav_rate_Dietas':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'A','Dietas',$sqlOps);
        break;
        case 'fav_Dietas':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'R','Dietas',$sqlOps);
        break;    
        case 'rate_Dietas':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'F','Dietas',$sqlOps);
        break;
        //show Favorites/Rate in Efforts
        case 'fav_rate_Esfuerzo':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'A','Esfuerzo',$sqlOps);
        break;
        case 'fav_Esfuerzo':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'R','Esfuerzo',$sqlOps);
        break;    
        case 'rate_Esfuerzo':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'F','Esfuerzo',$sqlOps);
        break; 
        //actions: DoFav, DoNotFav, Rate
        case 'FavAction':
            $sql0 = "CALL sp_fav_rate_exist('".$_POST['idCont']."','".$idUser."')";
            $result0 = $sqlOps->sql_single_row($sql0);
            if($result0 == ''){
                $sql1 = "CALL sp_fav_rate_add('".$_POST['idCont']."','".$idUser."',0,1)";
                $sqlOps->sql_exec_op($sql1);
            }
            else
            {
                $sql2 = "CALL sp_fav_rate_update(".$_POST['idCont'].",'FAVORITE',1)";
                $sqlOps->sql_exec_op($sql2);
            }
        break;
        case 'NotFavAction':
            $sql3 = "CALL sp_fav_rate_update(".$_POST['idCont'].",'FAVORITE',0)";
            $sqlOps->sql_exec_op($sql3);
        break;
        case 'RateAction':
            $sql4 = "CALL sp_fav_rate_exist('".$_POST['idCont']."','".$idUser."')";
            $result0 = $sqlOps->sql_single_row($sql4);
            if($result0 == '')
            {
                $sql5 = "CALL sp_fav_rate_add('".$_POST['idCont']."','".$idUser."','".$_POST['star']."',0)";
                $sqlOps->sql_exec_op($sql5);
            }
            else
            {
                $sql6 = "CALL sp_fav_rate_update(".$_POST['idCont'].",'RATE','".$_POST['star']."')";
                $sqlOps->sql_exec_op($sql6);
            }
        break;
    }
    echo $output == '' ? '' : $output;
    
