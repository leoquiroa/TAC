<?php
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    include "../../php/User/general.php";
    $generalFn = new generalUserFunctions();
    
    session_start();
    $idCoach = $_SESSION['idPerson'];
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    switch ($type_data){
        case 'fillActivityDdl':
            $output = $generalFn->getList(
                "CALL sp_catalog_list('NombreDeClase')",
                "Clase",
                "ddl_activity",
                $sqlOps,
                true);
        break;
        case 'checkActivity':
            $sql = "CALL sp_assistance_check("
                . "'".$idCoach."',"
                . "'".$_POST['idClass']."',"
                . "'".$_POST['idPerson']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        case 'showUserInfo':
            $sql = "CALL sp_person_list('".$_POST['idUser']."')";
            $row = $sqlOps->sql_single_row($sql);
            if($row != ''){
                $output = '
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="jumbotron">'.$row["Nombre"].'</div>
                        </div>
                        <div class="col-sx-4">
                            <img src="../../Multimedia/img/people/'.$row["Foto"].'" class="img-responsive" width="100px" height="100px">
                        </div>
                    </div>
                    <div id="id_div" style="display: none;">'.$_POST['idUser'].'</div>';
            }
        break;
    }
    
    echo $output == '' ? '' : $output;
