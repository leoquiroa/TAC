<?php
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    include "../../php/User/general.php";
    $generalFn = new generalUserFunctions();
    
    session_start();
    $idUser = $_SESSION['idPerson'];
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    switch ($type_data)
    {
        //initial show Article
        case 'ddl_article':
            $output = $generalFn->getList(
                    "CALL sp_catalog_list('CategoriaDeArticulo')",
                    "Categoria",
                    "ddl_type_article",
                    $sqlOps,
                    true);
        break;
        case 'table_article': 
            if($_POST['typeRecipeID']>0){
                $lcl =$sqlOps->sql_single_row("CALL sp_digitalcontent_count('Recetas','".$_POST['typeRecipeID']."','.$idUser.')");
                $noStar = (int)($lcl['TOTAL']*5)+1;
            }
            else{ $noStar = 1; }
            
            $output = $generalFn->TableContent(
                    'Articulos',
                    $_POST['typeArticleID'],
                    $idUser,
                    12,
                    'articulos',
                    'articleScroll',
                    $noStar,
                    $sqlOps);
        break;
        case 'signs_fav_rate_article':
            $output = $generalFn->FavRateSigns("Mis articulos","Articulos");
        break;
        //initial show Recipe
        case 'ddl_recipe':
            $output = $generalFn->getList(
                    "CALL sp_catalog_list('CategoriaDeReceta')",
                    "Receta",
                    "ddl_type_recipe",
                    $sqlOps,
                    true);
        break;
        case 'table_recipe':
            if($_POST['typeArticleID']>0)
            {
                $lcl =$sqlOps->sql_single_row("CALL sp_digitalcontent_count('Articulos','".$_POST['typeArticleID']."','.$idUser.')");
                $noStar = (int)($lcl['TOTAL']*5)+1;
            }
            else{ $noStar = 1; }

            $output = $generalFn->TableContent(
                    'Recetas',
                    $_POST['typeRecipeID'],
                    $idUser,
                    12,
                    'recetas',
                    'recipeScroll',
                    $noStar,
                    $sqlOps);
        break;
        case 'signs_fav_rate_recipe':
            $output = $generalFn->FavRateSigns("Mis recetas","Recetas");
        break;
        //show Favorites/Rate in Article
        case 'fav_rate_Articulos':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'A','Articulos',$sqlOps);
        break;
        case 'fav_Articulos':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'R','Articulos',$sqlOps);
        break;    
        case 'rate_Articulos':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'F','Articulos',$sqlOps);
        break; 
        //show Favorites/Rate in Recipe
        case 'fav_rate_Recetas':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'A','Recetas',$sqlOps);
        break;
        case 'fav_Recetas':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'R','Recetas',$sqlOps);
        break;    
        case 'rate_Recetas':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'F','Recetas',$sqlOps);
        break; 
        //actions: DoFav, DoNotFav, Rate
        case 'FavAction':
            $sql0 = "CALL sp_fav_rate_exist('".$_POST['idCont']."','".$idUser."')";
            $result0 = $sqlOps->sql_single_row($sql0);
            if($result0 == ''){
                $sql1 = "CALL sp_fav_rate_add('".$_POST['idCont']."','".$idUser."',0,1)";
                $sqlOps->sql_exec_op($sql1);
            }else{
                $sql2 = "CALL sp_fav_rate_update(".$_POST['idCont'].",'FAVORITE',1)";
                $sqlOps->sql_exec_op($sql2);
            }
        break;
        case 'NotFavAction':
            $sql3 = "CALL sp_fav_rate_update(".$_POST['idCont'].",'FAVORITE',0)";
            $sqlOps->sql_exec_op($sql3);
        break;
        case 'RateAction':
            $sql4 = "CALL sp_fav_rate_exist('".$_POST['idCont']."','".$idUser."')";
            $result0 = $sqlOps->sql_single_row($sql4);
            if($result0 == ''){
                $sql5 = "CALL sp_fav_rate_add('".$_POST['idCont']."','".$idUser."','".$_POST['star']."',0)";
                $sqlOps->sql_exec_op($sql5);
            }else{
                $sql6 = "CALL sp_fav_rate_update(".$_POST['idCont'].",'RATE','".$_POST['star']."')";
                $sqlOps->sql_exec_op($sql6);
            }
        break;
    }
    echo $output == '' ? '' : $output;