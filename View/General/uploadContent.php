<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])){header("Location:../General/login.php");}
    if($_SESSION['typePerson']!='Administrador'&&$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Nutricionista - Contenido Creado</title>
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/jasny-bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/verticalScroll.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <!-- ################################################# MENU ################################################# -->
    <!-- Navigation -->    
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <!-- Top Menu Items -->
        <div id="MenuTopDiv"></div>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div id="MenuContentDiv"></div>
    </nav>
    <!-- ################################################# MENU ################################################# -->        
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="white-div"></div>
            <div id="dummy_div"></div>
            <div style="font-size: 24px; text-align: center; color: #22CEDC;">
                <i class="fa fa-balance-scale"></i> DIETAS - 
                <i class="fa fa-heartbeat"></i> PRUEBAS DE ESFUERZO
            </div>
            <br/>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                        <strong>Información general</strong>
                    </div>
                    <hr/>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Seleccione</span>
                            <span class="fileinput-exists">Cambiar</span>
                            <input type="file" name="..." id="fileToUpload">
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Quitar</a>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-3">
                    <div id="content_div"></div>
                </div>
                <div class="col-md-5">
                    <input type="text" class="form-control" id="txt_title" placeholder="Titulo del contenido">
                </div>
                <div class="col-md-2"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-users" aria-hidden="true"></i>
                        <strong>Público objetivo</strong>
                    </div>
                    <hr/>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-3">
                    <input type="checkbox" id="all_places_check" value="0"> Todas las sedes
                    <hr/>
                    <div id="place_div"></div>
                </div>
                <div class="col-md-5">
                    <div id="table_div"></div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-calendar-times-o" aria-hidden="true"></i>
                        <strong>Visibilidad por tiempo</strong>
                    </div>
                    <hr/>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="txt_date_from" placeholder="Del">
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="txt_date_to" placeholder="Al">
                </div>
                <div class="col-md-2"></div>
            </div>  
            <br/>
            <div class="row">
                <div class="col-md-5"></div>
                <div class="col-md-2" style="text-align: center">
                    <button type="button" class="btn btn-block btn-success" id="btn_upload_recipe">
                        <span class="fa fa-upload"></span> Subir
                    </button>
                </div>
                <div class="col-md-5"></div>
            </div>
        </div>
    </div>
    <script src="../../Controller/js/External/pdf.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>       
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/jasny-bootstrap.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/verticalScroll.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/uploadContent.js" type="text/javascript"></script>    
</body>
</html>
