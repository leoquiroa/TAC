<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])){header("Location:../General/login.php");}
    if($_SESSION['typePerson']!='Administrador'&&$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Nutricionista - Eventos</title>
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/jasny-bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-clockpicker.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <!-- ################################################# MENU ################################################# -->
    <!-- Navigation -->    
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <!-- Top Menu Items -->
        <div id="MenuTopDiv"></div>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div id="MenuContentDiv"></div>
    </nav>
    <!-- ################################################# MENU ################################################# -->        
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="white-div"></div>
            <div style="font-size: 24px; text-align: center; color: #22CEDC;">
                <i class="fa fa-users"></i> EVENTOS
            </div>
            <br/>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="txt_title" placeholder="Titulo"><br/>
                    <input type="text" class="form-control" id="txt_date" placeholder="Fecha"><br/>
                    <input type="text" class="form-control" id="txt_place" placeholder="Lugar"><br/>
                    <div class="input-group clockpicker" data-autoclose="true" id="input_hour">
                        <input type="text" class="form-control" value="" placeholder="Hora" id="txt_hour">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                    <br/>
                    <div id="category_div"></div>
                    <br/>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="txt_date_from" placeholder="Del">
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="txt_date_to" placeholder="Al">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width:260px; height:260px">
                            <img src="../../Multimedia/img/people/no_image.png" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 260px; height: 260px;" id="new_photo_form_preview"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Buscar</span>
                                <span class="fileinput-exists">¿Otra?</span>
                                <input type="file" name="..." id="ephoto-upload" >
                            </span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Cancelar</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-5"></div>
                <div class="col-md-2">
                    <button class="btn btn-block btn-success" type="button" id="btn_events">Enviar</button>
                </div>
                <div class="col-md-5"></div>
            </div>
            <div id="dummy_div"></div>
        </div>
    </div>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>       
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/jasny-bootstrap.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-clockpicker.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/events.js" type="text/javascript"></script>        
</body>
</html>
