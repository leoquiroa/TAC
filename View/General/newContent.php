<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])){header("Location:../General/login.php");}
    if($_SESSION['typePerson']!='Administrador'&&$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Nutricionista - Contenido Nuevo</title>
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Tools/wysiwyg/summernote.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <!-- ################################################# MENU ################################################# -->
    <!-- Navigation -->    
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <!-- Top Menu Items -->
        <div id="MenuTopDiv"></div>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div id="MenuContentDiv"></div>
    </nav>
    <!-- ################################################# MENU ################################################# -->        
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="white-div"></div>
            <div id="dummy_div"></div>
            <div style="font-size: 18px; text-align: center; color: #22CEDC;">
                <i class="fa fa-cutlery"></i> RECETAS - <i class="fa fa-book"></i> ARTICULOS
            </div>
            <br/>
            <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <div id="type_ddl_div"></div>
                    </div>
                    <div class="col-md-3">
                        <div id="category_ddl_div"></div>
                    </div>
                    <div class="col-md-3"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-4">
                    <div id="title_ddl_div"></div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="titleNew" placeholder="Titulo de la entrada">
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-success" id="btn_new" style="width: 100%;">Nuevo</button>
                </div>
                <div class="col-md-3"></div>
            </div>  
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="txt_date_from" placeholder="Del">
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="txt_date_to" placeholder="Al">
                </div>
                <div class="col-md-3"></div>
            </div>  
            <br/>
            <form action="#" novalidate>
                <div class="form-group">
                    <textarea name="text" class="summernote" id="contents" title="Contents"></textarea>
                </div>
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-success" id="btn_save" style="width: 100%;">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-danger" id="btn_delete" style="width: 100%;">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="col-md-5"></div>
                </div>
            </form>
        </div>
    </div>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>       
    <script src="../../Tools/wysiwyg/summernote.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/newContent.js" type="text/javascript"></script>            
</body>
</html>
