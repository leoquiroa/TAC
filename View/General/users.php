<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])){header("Location:../General/login.php");}
    if($_SESSION['typePerson']!='Administrador'&&$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Nutricionista - Ususario - Inicio</title>    
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/jasny-bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/Form.css" rel="stylesheet" type="text/css"/>  
    <link href="../../Controller/css/General/userControl.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <div id="MenuTopDiv"></div>
        <div id="MenuContentDiv"></div>
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="white-div"></div>
            <div class="bigTitle">
                <i class="fa fa-user"></i> USUARIOS
            </div>
            <br/>
            <div id="mask">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <ul class="nav nav-tabs internalMenu">
                            <li><a href="users.php">Listado</a></li>
                            <li><a href="userDetails.php">Detalles</a></li>
                            <li><a href="userControl.php">Control</a></li>
                            <li><a href="userFollowing.php">Seguimiento</a></li>
                            <li><a href="userBadges.php">Badges</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <div id="filter_div"></div>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-1" style="color: green;">
                        <span class="fa fa-user-plus fa-2x" id="showAddForm" style="cursor:pointer"></span>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <br/> 
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div style="color: gray; font-size: 20px;">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <strong>Listado</strong>
                        </div>
                        <div id="table_div"></div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div id="dummy_div"></div>
            </div>
            <!-- add a new user -->
            <div id="addform" class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" id="new_name" placeholder="Nombre completo" class="form-control nuevo"/>
                        <input type="text" id="new_nick" placeholder="Nick" class="form-control nuevo"/>
                        <div id="new_user_gender_div" ></div>
                        <input type="text" id="new_height" placeholder="Altura" class="form-control nuevo"/>
                        <input type="text" id="new_birthday" placeholder="Fecha de Nac. - dd/mm/yyyy" class="form-control nuevo"/>
                        <input type="text" id="new_phone" placeholder="No. de celular" class="form-control nuevo"/>
                        <input type="text" id="new_email" placeholder="Correo electrónico" class="form-control nuevo"/>
                        <div id="new_user_place_div" ></div>
                    </div>
                    <div class="col-md-6">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width:200px; height:190px">
                                <img src="../../Multimedia/img/people/no_image.png" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;" id="new_photo_form_preview"></div>
                            <div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileinput-new">Buscar</span>
                                    <span class="fileinput-exists">Mejor otra</span>
                                    <input type="file" name="..." id="new_photo_form">
                                </span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Cancelar</a>
                            </div>
                        </div>
                        <div id="new_user_type_div" ></div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="nuevo_btns">
                                    <button class="btn btn-success" id="performsave"><i class="fa fa-save"></i> Guardar</button>
                                    <button class="btn btn-danger" id="cancelsave"><i class="fa fa-remove"></i> Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- delete a user -->
            <div id="deleteform" class="form-group">
                <textarea id="reason" placeholder="Razón" class="form-control nuevo" rows="4"></textarea>
                <div class="nuevo_btns">
                    <button class="btn btn-success" id="deactivateUser"><i class="fa fa-trash-o"></i> Borrar</button>
                    <button class="btn btn-danger" id="canceldelete"><i class="fa fa-remove"></i> Cancelar</button>
                </div>
            </div>            
        </div>
    </div>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/jasny-bootstrap.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/moment.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/jquery.dataTables.1.10.12.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/dataTables.responsive.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/notify.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/users.js" type="text/javascript"></script>        
</body>
</html>

