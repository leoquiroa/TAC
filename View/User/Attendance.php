<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Integrante'){header("Location:../General/login.php");}
?> 
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title>Asistencia</title>
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/zabuto_calendar.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/verticalScroll.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/colorCalendar.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/User/ownTable4col.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/User/ownStyle.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/User/leftMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <!-- ################################################# MENU ################################################# -->
    <div id="mySidenav" class="sidenav">
        <a href="../User/Home.php"><img src="../../Multimedia/img/MenuIcons/inicio.png" height="30" alt=""><br>Inicio</a>
        <a href="../User/Calendar.php"><img src="../../Multimedia/img/MenuIcons/calendarios.png" height="30" alt=""><br>Calendario</a>
        <a href="../User/Attendance.php"><img src="../../Multimedia/img/MenuIcons/asistencias.png" height="30" alt=""><br>Asistencias</a>
        <a href="../User/Control.php"><img src="../../Multimedia/img/MenuIcons/controlnutricional.png" height="30" alt=""><br>Control Nutricional</a>
        <a href="../User/Diet_Effort.php"><img src="../../Multimedia/img/MenuIcons/dietas.png" height="30" alt=""><br>Dietas & Pruebas</a>
        <a href="../User/Routine.php"><img src="../../Multimedia/img/MenuIcons/videos.png" height="30" alt=""><br>Rutinas</a>
        <a href="../User/Article_Recipe.php"><img src="../../Multimedia/img/MenuIcons/articulosrecetas.png" height="30" alt=""><br>Articulos & Recetas</a>
        <a href="../General/logout.php"><img src="../../Multimedia/img/MenuIcons/salir.png" height="30" alt=""><br>Salir</a>
    </div>
    <div id="top">
        <div class="col-xs-10" id="div_logo">
            <a href="../User/Home.php">
                <img src="../../Multimedia/img/LogoTigofit.png" alt="" id="logo">
            </a>
        </div>
        <div class="col-xs-2" id="div_three_bars">
            <span onclick="openCloseNavMenu()">
                <i id="three_bars" class="fa fa-bars" aria-hidden="true"></i>
            </span>
        </div>
    </div>
    <!--<div id="myAlert" class="alert alert-info" hidden="true"></div>-->
    <div class="col-xs-12 white-div"></div>
    <!-- ################################################# MENU ################################################# -->
    <div style="padding-left: 10px; padding-right: 10px;">
        <div style="font-size: 16px;">
            <img src="../../Multimedia/img/mountainIcon.png" height="30" alt=""> ASISTENCIAS
        </div>
        <br/>
        <div id="ddls_div"></div>
        <br/>
        <div id="graph_div"></div>
        <br/>
        <div style="font-size: 16px; color:#EE6B73;">
            <i class="fa fa-exclamation-circle" aria-hidden="true"></i> <b>DETALLE DE ASISTENCIAS</b>
        </div>
        <br/>
        <div id="AttendTable_div"></div>
        <div style="font-size: 16px; color:#22CEDC;">
            <i class="fa fa-calendar" aria-hidden="true"></i> <b>CALENDARIO DE ASISTENCIAS</b>
        </div>
        <!--<div id="AttendCalendar_div"></div>-->             
        <br/>
        <div id="CalendarAttend"></div>
        <br/>
        <div style="background-color: lightgray;"><br/></div>
        <br/>
        <div style="font-size: 16px; color:#CCB05E;">
            <img src="../../Multimedia/img/whistle.png" height="30" alt=""> <b> CALIFICA A TU ENTRENADOR</b>
        </div>
        <br/>
        <div id="RateCoach_div"></div>
        <div id="dummy_div"></div>
        <br/>
    </div>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/Chart.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/zabuto_calendar.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/verticalScroll.js" type="text/javascript"></script>
    <script src="../../Controller/js/User/ownRating.js" type="text/javascript"></script>
    <script src="../../Controller/js/User/Attendance.js" type="text/javascript"></script>
    <script src="../../Controller/js/User/leftMenu.js" type="text/javascript"></script>        
</body>
</html>
