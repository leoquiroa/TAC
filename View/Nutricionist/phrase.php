<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Nutricionista - Frase</title>        
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/verticalScroll.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <nav class="navbar navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <div id="MenuTopDiv"></div>
        <div id="MenuContentDiv"></div>
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="white-div"></div>
            <div style="font-size: 24px; text-align: center; color: #22CEDC;">
                <i class="fa fa-commenting-o"></i> FRASES
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-comment" aria-hidden="true"></i>
                        <strong>Nueva</strong>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="txt_title" placeholder="Titulo">
                </div>
                <div class="col-md-3">
                    <div id="ddl_category_new_div"></div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <textarea class="form-control" rows="5" id="comment" placeholder="Frase Motivational"></textarea>
                    <hr/>
                </div>
                <div class="col-md-3"></div>
            </div>  
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-comment" aria-hidden="true"></i>
                        <strong>Existente</strong>
                    </div>
                </div>
                <div class="col-md-3">
                    <div id="ddl_category_existent_div"></div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div id="table_existent_div"></div>
                    <hr/>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-comment" aria-hidden="true"></i>
                        <strong>Frase Aleatoria</strong>
                    </div>
                </div>
                <div class="col-md-3">
                    <input type="checkbox" id="mainCheck" value="0">
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <hr/>
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-calendar-times-o" aria-hidden="true"></i>
                        <strong>Visibilidad por tiempo</strong>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="txt_date_from" placeholder="Del">
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="txt_date_to" placeholder="Al">
                </div>
                <div class="col-md-3"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-5"></div>
                <div class="col-md-2">
                    <button class="btn btn-block btn-success" type="button" id="btn_phrase">Enviar</button>
                </div>
                <div class="col-md-5"></div>
            </div>
            <div id="dummy_div"></div>
        </div>
    </div>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>       
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/notify.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/verticalScroll.js" type="text/javascript"></script>
    <script src="../../Controller/js/Nutricionist/phrase.js" type="text/javascript"></script>        
</body>
</html>
