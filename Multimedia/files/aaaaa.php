
                <html>
                    <head>
                        <meta charset="UTF-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="description" content="TAC Peakfit site">
                        <meta name="author" content="@leoquiroa">
                        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
                        <title>Usuario</title>
                        <!--Third CSS-->
                        <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
                        <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
                        <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
                        <!--Own CSS-->
                        <link href="../../Controller/css/User/leftMenu.css" rel="stylesheet" type="text/css"/>
                        <!--Third JS-->
                        <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
                        <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
                        <!--Own JS-->
                        <script src="../../Controller/js/User/leftMenu.js" type="text/javascript"></script>
                    </head>
                    <body>
                        <!-- ################################################# MENU ################################################# -->
                        <div id="mySidenav" class="sidenav">
                            <a href="../../View/User/home.php"><i class="fa fa-home" aria-hidden="true"></i><br>Inicio</a>
                            <a href="../../View/User/Calendar.php"><i class="fa fa-calendar-o" aria-hidden="true"></i><br>Calendario</a>
                            <a href="../../View/User/Attendance.php"><img src="../img/mountainIcon.png" height="30" alt="" class="invert"><br>Asistencias</a>
                            <a href="../../View/User/Control.php"><img src="../img/scaleIcon.png" height="30" alt="" class="invert"><br>Control Nutricional</a>
                            <a href="../../View/User/Diet_Effort.php"><i class="fa fa-info-circle" aria-hidden="true"></i><br>Dietas & Pruebas</a>
                            <a href="../../View/User/Routine.php"><i class="fa fa-video-camera" aria-hidden="true"></i><br>Rutinas</a>
                            <a href="../../View/User/Article_Recipe.php"><i class="fa fa-book" aria-hidden="true"></i> <i class="fa fa-cutlery" aria-hidden="true"></i><br>Articulos & Recetas</a>
                            <a href="../../View/General/logout.php"><i class="fa fa-power-off" aria-hidden="true"></i><br>Salir</a>
                        </div>
                        <div id="top">
                            <div class="col-xs-10" id="div_logo">
                                <img src="../img/tigoMenu.png" alt="" id="logo">
                            </div>
                            <div class="col-xs-2" id="div_three_bars">
                                <span onclick="openCloseNavMenu()">
                                    <i id="three_bars" class="fa fa-bars" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 white-div"></div>
                        <!-- ################################################# MENU ################################################# -->
                        <div style="padding-left: 10px; padding-right: 10px;"><h1 style="padding: 20px 0px 0px; margin-top: 0px; margin-bottom: 0px;"><font face="Arial Black" style="background-color: rgb(0, 0, 255);">Lorem Ipsum Generator</font></h1><p><br></p><p style="font-size: 14px; text-align: justify;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;<span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span></p><p><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Wiktionary_small.svg/350px-Wiktionary_small.svg.png" alt="Image result for image small" style="width: 25%; float: left;"></p><p style="font-size: 14px; text-align: justify;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.<span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span></p><p><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Argent_a_fylfot_azure.svg/250px-Argent_a_fylfot_azure.svg.png" alt="Related image" style="font-size: 14px; width: 100%; float: right;"></p>        
                        </div>
                    </body>
                </html>